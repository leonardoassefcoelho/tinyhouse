<?php
/* Template Name: Page My Account */
get_header();
$breadcrumb_args = array(
    'wrap_before' => '<nav class="woocommerce-breadcrumb">',
    'wrap_after' => '</nav>',
    'delimiter' => '<span> > </span>'
);
?>

<div class="my-account-content">
    <?php get_template_part('template-parts/cart/hero');  ?>
    <div class="padding-container">
        <?php woocommerce_breadcrumb($breadcrumb_args); ?>
        <h1 class="title h1"><?= get_the_title() ?></h1>
        <?= the_content(); ?>
    </div>
</div>

<?php
get_footer();
