<?php
$args = array(
    'post_type' => 'radios',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);

$query = new WP_Query($args);

if ($query->have_posts()) :
?>
    <div class="posts-wrapper">
        <div class="posts-container padding-container">
            <div class="slider-wrapper">
                <?php
                while ($query->have_posts()) : $query->the_post();
                ?>
                    <div class="post-card">
                        <div class="img-wrapper">
                            <?php
                            $thumbnail = tinyhouse_image_sanitize(get_post_thumbnail_id(), "full");
                            ?>
                            <a class="title" aria-label="<?= get_the_title() ?>" href="<?= get_permalink() ?>"><img loading="lazy" class="hero-image" src="<?= $thumbnail['src'] ?>" alt="<?= $thumbnail['alt'] ?>" title="<?= $thumbnail['title'] ?>" <?= ($thumbnail['srcset'] ? 'srcset="' . $thumbnail['srcset'] . '"' : '') ?>></a>
                        </div>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </div>
<?php
endif;
wp_reset_postdata();
?>