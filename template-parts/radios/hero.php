<?php
$pageID = get_the_id();
?>

<div class="padding-container journals-hero">
    <?php
    $radio_left_badge = get_field('radio-left-badge');
    if ($radio_left_badge) :
        echo file_get_contents($radio_left_badge);
    endif;
    ?>

    <div class="hero-content">
        <h1 class="h1 title"><?= get_the_title($pageID) ?></h1>
        <div class="description"><?= get_post_field('post_content', $pageID) ?></div>
    </div>

    <?php
    $radio_right_badge = get_field('radio-right-badge');
    if ($radio_right_badge) :
        echo file_get_contents($radio_right_badge);
    endif;
    ?>
</div>