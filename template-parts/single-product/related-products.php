<section class="related-products-section padding-container">
    <h2 class="h2 title">You may also like</h2>
    <div class="products-wrapper">
        <?php
        global $product;
        $category = get_the_terms($product->get_id(), 'product_cat')[0];
        if (!get_field('product-related-product-auto') && have_rows('product-related-repeater')) :
            while (have_rows('product-related-repeater')) : the_row();
                get_template_part('template-parts/common/product-card', null, wc_get_product(get_sub_field('product-related-product')));
            endwhile;
        else :
            $args = array(
                'post_type'      => 'product',
                'posts_per_page' => 4,
                'ignore_sticky_posts' => true,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'term_id',
                        'terms' => $category->term_id
                    ),
                ),
                'post__not_in' => array(get_the_ID()),
            );

            $products = new WP_Query($args);

            if ($products->have_posts()) :
                while ($products->have_posts()) : $products->the_post();
                    global $product;
                    if ($product->is_purchasable() && $product->is_in_stock()) :
                        get_template_part('template-parts/common/product-card', null, $product);
                    endif;
                endwhile;
            endif;
            wp_reset_postdata();
        endif;
        ?>
    </div>
</section>