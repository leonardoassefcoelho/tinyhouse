<?php
global $product;
$category = get_the_terms($product->get_id(), 'product_cat')[0];
$breadcrumb_args = array(
    'wrap_before' => '<nav class="woocommerce-breadcrumb padding-container tablet">',
    'wrap_after' => '</nav>',
    'delimiter' => '<span> > </span>'
);
$product_type = $product->get_type();
$product_id = $product->get_ID();
?>
<div class="product-content">

    <div class="product-infos">
        <?php
        woocommerce_breadcrumb($breadcrumb_args); ?>

        <a class="category" href="<?= get_term_link($category->term_id) ?>" alt="<?= $category->name ?>"><?= $category->name ?></a>
        <h1 class="h2 title" itemprop="name"><?= $product->get_title() ?></h1>
        <div itemprop="brand" itemtype="https://schema.org/Brand" itemscope hidden>
            <meta itemprop="name" content="TinyHouse Chocolate" />
        </div>
        <p class="price" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
            <link itemprop="url" href="<?= $product->get_permalink() ?>" />
            <link itemprop="availability" href="<?= $product->is_in_stock() ? "https://schema.org/InStock" : "https://schema.org/OutOfStock" ?>">
            <span class="woocommerce-Price-amount amount" itemprop="price" content="<?= number_format($product->get_price(), 2, '.', '') ?>"><bdi><span class="woocommerce-Price-currencySymbol" itemprop="priceCurrency" content="<?= get_woocommerce_currency() ?>"><?= get_woocommerce_currency_symbol() ?></span><?= number_format($product->get_price(), 2, '.', '') ?></bdi></span>
        </p>
        <?php
        if ($product->is_type('variable')) {
            woocommerce_variable_add_to_cart();
        }

        if ($product_type != 'cpb_custom_product_boxes') :
            get_template_part('template-parts/common/components/counter', '', array(
                'product_id'  => $product->get_id()
            ));
        ?>
            <div class="short-description" itemprop="description">
                <?= wpautop($product->get_short_description()) ?>
            </div>
            <div class="product-labels">
                <?php
                if (have_rows('product-labels-repeater')) :
                    while (have_rows('product-labels-repeater')) : the_row();
                        echo file_get_contents(get_field('product-labes-label', get_sub_field('product_label')));
                    endwhile;
                endif;
                ?>
            </div>
        <?php endif; ?>
        <div class="long-description">
            <h2 class="about-this-bar">
                <?php
                $product_about_type = get_field('product-type');
                if ($product_about_type != '') :
                    echo sprintf("About this %s", $product_about_type);
                else :
                    if ($product_type == 'cpb_custom_product_boxes') :
                        echo 'About this bundle';
                    else :
                        echo 'About this bar';
                    endif;
                endif;
                ?>
            </h2>
            <?= wpautop($product->get_description()) ?>
        </div>
        <?php
        if ($product_type == 'cpb_custom_product_boxes') :
        ?>
            <div class="custom-boxes-slots" data-product-id="<?= $product->get_id() ?>">
                <?php
                $boxes = get_post_meta($product_id, 'cpb_product_boxes_max_items', true);
                $cat_ids = get_post_meta($product_id, 'cpb_product_boxes_categories', true);
                $product_ids = get_post_meta($product_id, 'cpb_product_boxess_items', true);
                wp_nonce_field('cpb_product_boxes_nonce', 'cpb_product_boxes_nonce');
                ?>
                <div class="slots-wrapper">
                    <?php
                    for ($i = 0; $i < $boxes; $i++) :
                    ?>
                        <div class="product-slot"></div>
                    <?php
                    endfor;
                    ?>
                </div>
                <p class="empty-slots">Clean All</p>
                <?php get_template_part('template-parts/common/components/counter', '', array(
                    'product_id'  => $product->get_id()
                ));
                ?>
            </div>
            <div class="product-labels">
                <?php
                if (have_rows('product-labels-repeater')) :
                    while (have_rows('product-labels-repeater')) : the_row();
                        echo file_get_contents(get_field('product-labes-label', get_sub_field('product_label')));
                    endwhile;
                endif;
                ?>
            </div>
        <?php
        endif;
        ?>
    </div>
</div>
<?php
if ($product_type == 'cpb_custom_product_boxes') :
?>
    <div class="custom-box-grid">
        <?php
        $controller = CPB_Custom_Product_Boxes_Frontend_Controller::instance();
        $cat_ids = get_post_meta($product_id, 'cpb_product_boxes_categories', true);
        $product_ids = get_post_meta($product_id, 'cpb_product_boxess_items', true);
        $products = $controller::cpb_get_products($product_id, $product_ids, $cat_ids, null, 1);

        if ($products->have_posts()) :
            while ($products->have_posts()) : $products->the_post();
                global $product;
                if (!($product->is_purchasable() && $product->is_in_stock()) && 'yes' == get_post_meta($product_id, 'cpb_product_boxes_hide_unpurchasable', true)) {
                    continue;
                }
                get_template_part('template-parts/common/custom-box-card', null, $product);
            endwhile;
        endif;
        ?>
    </div>
<?php endif; ?>