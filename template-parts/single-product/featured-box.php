<?php if (is_null(get_field('products-featured-show', 'option')) || get_field('products-featured-show', 'option')) : ?>
    <section class="products-featured-box">
        <?php
        $featured_image = tinyhouse_image_sanitize(get_field('products-featured-image', 'option'), "full");
        ?>
        <div class="img-wrapper <?= get_field('products-featured-image-filter', 'option') ? 'grayscale' : '' ?>">
            <img loading="lazy" class="featured-image" src="<?= $featured_image['src'] ?>" alt="<?= $featured_image['alt'] ?>" title="<?= $featured_image['title'] ?>" <?= ($featured_image['srcset'] ? 'srcset="' . $featured_image['srcset'] . '"' : '') ?>>
        </div>

        <?= (get_field('products-featured-icon', 'option') && get_field('products-featured-what-to-show', 'option') == 'icon') ? file_get_contents(get_field('products-featured-icon', 'option')) : '' ?>
        <div class="content-wrapper">
            <?php if (get_field('products-featured-what-to-show', 'option') == 'text') : ?>
                <p class="h2 desription"><?= get_field('products-featured-text', 'option') ?></p>
            <?php endif; ?>
            <?php if (!is_null(get_field('products-featured-show-button', 'option')) || get_field('home-conversion-button-show', 'option')) : ?>
                <a class="button light default" target="<?= get_field('products-featured-button-target', 'option') ?>" href="<?= get_field('products-featured-button-link', 'option') ?>" alt="<?= get_field('products-featured-button-text', 'option') ?>"><?= get_field('products-featured-button-text', 'option') ?></a>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>