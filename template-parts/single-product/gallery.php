<?php
global $product;
$gallery_imgs = $product->get_gallery_image_ids();
array_unshift($gallery_imgs, $product->get_image_id());
?>

<div class="product-gallery">
    <?php
    foreach ($gallery_imgs as $gallery_img) :

        $product_image = tinyhouse_image_sanitize($gallery_img, "full");
    ?>
        <div class="img-wrapper">
            <img itemprop="image" loading="lazy" class="hero-image" src="<?= $product_image['src'] ?>" alt="<?= $product_image['alt'] ?>" title="<?= $product_image['title'] ?>" <?= ($product_image['srcset'] ? 'srcset="' . $product_image['srcset'] . '"' : '') ?>>
        </div>
    <?php
    endforeach;
    ?>
</div>