<section class="stores-wrapper padding-container top-header-distance">
    <h1 class="title h1"><?= the_title() ?></h1>
    <?php
    $breadcrumb_args = array(
        'wrap_before' => '<nav class="woocommerce-breadcrumb">',
        'wrap_after' => '</nav>',
        'delimiter' => '<span> > </span>'
    );
    ?>

    <?php woocommerce_breadcrumb($breadcrumb_args); ?>

    <div class="content-wrapper">
        <div class="stores-list-wrapper">
            <nav class="stores-navigation">
                <ul>
                    <li class="stores active">Location</li>
                    <li class="online">Online</li>
                </ul>
            </nav>

            <div class="stores-list active">
                <?php if (have_rows('stockists-stores-states-repeater')) :
                    while (have_rows('stockists-stores-states-repeater')) : the_row(); ?>
                        <div class="state-wrapper">
                            <div class="state-title-wrapper">

                                <h2 class="h3 state-title"><?= get_sub_field('stockists-stores-state-name') ?></h2>
                            </div>
                            <?php if (have_rows('stockists-stores-citys-repeater')) :
                                while (have_rows('stockists-stores-citys-repeater')) : the_row(); ?>
                                    <div class="city-wrapper">
                                        <h3 class="city-title"><?= get_sub_field('stockists-stores-city-name') ?></h3>
                                        <?php if (have_rows('stockists-stores-repeater')) :
                                            while (have_rows('stockists-stores-repeater')) : the_row(); ?>
                                                <div class="store">
                                                    <h4 class="name"><?= get_sub_field('stockists-store-name') ?></h4>
                                                    <p class="address"><?= get_sub_field('stockists-store-address') ?></p>
                                                </div>
                                        <?php endwhile;
                                        endif; ?>
                                    </div>
                            <?php endwhile;
                            endif; ?>
                        </div>
                <?php endwhile;
                endif; ?>
            </div>
            <div class="stores-list online">
                <?php if (have_rows('stockists-online-stores-states-repeater')) :
                    while (have_rows('stockists-online-stores-states-repeater')) : the_row(); ?>
                        <div class="state-wrapper">
                            <div class="state-title-wrapper">

                                <h2 class="h3 state-title"><?= get_sub_field('stockists-stores-state-name') ?></h2>
                            </div>
                            <?php if (have_rows('stockists-stores-citys-repeater')) :
                                while (have_rows('stockists-stores-citys-repeater')) : the_row(); ?>
                                    <div class="city-wrapper">
                                        <h3 class="city-title"><?= get_sub_field('stockists-stores-city-name') ?></h3>
                                        <?php if (have_rows('stockists-stores-repeater')) :
                                            while (have_rows('stockists-stores-repeater')) : the_row(); ?>
                                                <div class="store">
                                                    <h4 class="name"><?= get_sub_field('stockists-store-name') ?></h4>
                                                    <a href="<?= get_sub_field('stockists-store-address') ?>" target="_blank" rel="nofollow" class="address">Shop now</a>
                                                </div>
                                        <?php endwhile;
                                        endif; ?>
                                    </div>
                            <?php endwhile;
                            endif; ?>
                        </div>
                <?php endwhile;
                endif; ?>
            </div>
        </div>



        <div class="map-wrapper" data-ajax-url="<?= admin_url('admin-ajax.php') ?>" data-page-id="<?= the_id() ?>" data-nonce="<?= wp_create_nonce('stockists_page') ?>">
            <div id="map" class="map"></div>
        </div>
    </div>
</section>