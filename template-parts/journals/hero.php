<?php
$pageID = get_option('page_for_posts', true);
?>

<div class="padding-container journals-hero">
    <div class="imgs-wrapper">
        <div class="img-wrapper back-image">
            <?php
            $hero_image = tinyhouse_image_sanitize(get_field('journals-hero-back-img', $pageID), "full");
            ?>
            <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
        </div>
        <div class="img-wrapper front-image">
            <?php
            $hero_image = tinyhouse_image_sanitize(get_field('journals-hero-front-img', $pageID), "full");
            ?>
            <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
        </div>
    </div>

    <?php
    $hero_badge = get_field("journals-hero-badge", $pageID);
    if ($hero_badge) :
        echo file_get_contents($hero_badge);
    endif;
    ?>

    <div class="hero-content">
        <h1 class="h1 title"><?= get_the_title($pageID) ?></h1>
        <div class="description"><?= get_post_field('post_content', $pageID) ?></div>
    </div>
</div>