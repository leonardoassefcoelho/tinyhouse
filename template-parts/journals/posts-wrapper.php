<?php
if (have_posts()) :
?>
    <div class="posts-wrapper">
        <?php
        $index = 0;
        while (have_posts()) : the_post();
            if ($index == 0) :
        ?>
                <div class="padding-container featured-post">
                    <h2 class="h2 title">Our posts</h2>
                    <div class="card">
                        <div class="img-wrapper">
                            <?php
                            $thumbnail = tinyhouse_image_sanitize(get_post_thumbnail_id(), "full");
                            ?>
                            <img loading="lazy" class="hero-image" src="<?= $thumbnail['src'] ?>" alt="<?= $thumbnail['alt'] ?>" title="<?= $thumbnail['title'] ?>" <?= ($thumbnail['srcset'] ? 'srcset="' . $thumbnail['srcset'] . '"' : '') ?>>
                        </div>
                        <div class="content-wrapper">
                            <h3 class="title"><?= get_the_title() ?></h3>
                            <p class="excerpt"><?= get_the_excerpt() ?></p>
                            <a class="button dark default" aria-label="Read more" href="<?= get_permalink() ?>">Read more</a>
                        </div>
                    </div>
                </div>
                <div class="posts-container padding-container">
                    <div class="slider-wrapper">
                    <?php
                else :
                    ?>
                        <div class="post-card">
                            <div class="img-wrapper">
                                <?php
                                $thumbnail = tinyhouse_image_sanitize(get_post_thumbnail_id(), "full");
                                ?>
                                <img loading="lazy" class="hero-image" src="<?= $thumbnail['src'] ?>" alt="<?= $thumbnail['alt'] ?>" title="<?= $thumbnail['title'] ?>" <?= ($thumbnail['srcset'] ? 'srcset="' . $thumbnail['srcset'] . '"' : '') ?>>
                            </div>
                            <div class="content-wrapper">
                                <a class="title" aria-label="<?= get_the_title() ?>" href="<?= get_permalink() ?>"><?= get_the_title() ?></a>
                                <p class="excerpt"><?= get_the_excerpt() ?></p>
                            </div>

                        </div>
                <?php
                endif;
                $index++;
            endwhile;
                ?>
                    </div>
                </div>
    </div>
<?php
endif;
?>