<div class="cart-hero">
    <?php
    $hero_image = tinyhouse_image_sanitize(get_field('cart-hero-image', get_option('woocommerce_cart_page_id')), "full");
    ?>
    <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>

    <div class="hero-content">
        <?php
        $cart_hero_logo = get_field('cart-hero-logo', get_option('woocommerce_cart_page_id'));
        if ($cart_hero_logo) :
            echo file_get_contents($cart_hero_logo);
        endif; ?>
        <h2 class="h1 title"><?= get_field('cart-hero-text', get_option('woocommerce_cart_page_id')) ?></h2>
    </div>
</div>