<?php if (is_null(get_field('cart-product-suggestions-show')) || get_field('cart-product-suggestions-show')) : ?>
    <section class="product-suggestions padding-container">
        <div class="content-wrapper">
            <h2 class="h2 title"><?= get_field('cart-product-suggestions-title') ?></h2>
        </div>
        <div class="products-wrapper">
            <?php
            if (have_rows('cart-product-suggestions-repeater')) :
                while (have_rows('cart-product-suggestions-repeater')) : the_row();
                    global $product;
                    $product = wc_get_product(get_sub_field('cart-product-suggestions-product'));
            ?>
                    <div class="product-item">
                        <div class="img-wrapper">
                            <?php
                            $product_image = tinyhouse_image_sanitize($product->get_image_id(), "full");
                            ?>
                            <img loading="lazy" class="conversion-image" src="<?= $product_image['src'] ?>" alt="<?= $product_image['alt'] ?>" title="<?= $product_image['title'] ?>" <?= ($product_image['srcset'] ? 'srcset="' . $product_image['srcset'] . '"' : '') ?>>
                        </div>
                        <a href="<?= get_permalink($product->get_id()) ?>" alt="<?= $product->get_name() ?>" class="product-name"><?= $product->get_name() ?></a>
                        <p class="product-price"><?= wc_price($product->get_price()) ?></p>
                        <?php get_template_part('template-parts/common/components/counter', '', array(
                            'product_id'  => $product->get_id()
                        )); ?>
                    </div>
            <?php endwhile;
            endif; ?>
        </div>
    </section>
<?php endif; ?>