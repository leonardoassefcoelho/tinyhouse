<?php if (is_null(get_field('home-badges-show')) || get_field('home-badges-show')) : ?>
    <section class="home-badges" id="badges">
        <h2 class="h2 title"><?= get_field('home-badges-title') ?></h2>
        <div class="badges-wrapper">
            <?php if (have_rows('home-badges-repeater')) :
                while (have_rows('home-badges-repeater')) : the_row(); ?>
                    <div class="badge-item">
                        <?= file_get_contents(get_sub_field('home-badges-logo')) ?>
                        <p class="description"><?= get_sub_field('home-badges-text') ?></p>
                    </div>
            <?php endwhile;
            endif; ?>
        </div>
    </section>
<?php endif; ?>