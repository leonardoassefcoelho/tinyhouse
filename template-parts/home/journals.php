<?php if (is_null(get_field('home-journals-show')) || get_field('home-journals-show')) : ?>
    <section class="home-journals">
        <div class="content-wrapper">
            <h2 class="h2 title"><?= get_field('home-journals-title') ?></h2>
            <p class="description"><?= get_field('home-journals-text') ?></p>
            <?php if (is_null(get_field('home-journals-button-show')) || get_field('home-journals-button-show')) : ?>
                <a class="button dark outlined desktop" href="<?= get_field('home-journals-button-url') ?>" alt="<?= get_field('home-journals-button-text') ?>"><?= get_field('home-journals-button-text') ?></a>
            <?php endif; ?>
        </div>
        <div class="journals-wrapper">
            <?php
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'ignore_sticky_posts' => true,
                'posts_per_page' => 4,
            );

            if (get_field('home-journals-post-type') == 'popular') :
                $args['meta_key'] = 'post_view_count';
                $args['orderby'] = 'meta_value_num';
            elseif (get_field('home-journals-post-type') == 'selection') :
                $args['post__in'] = (get_field('home-journals-post-selection'));
            endif;

            $the_query = new WP_Query($args);

            if ($the_query->have_posts()) :
                while ($the_query->have_posts()) : $the_query->the_post();
            ?>
                    <div class="journal-item">

                        <div class="img-wrapper">
                            <?php
                            $post_image = tinyhouse_image_sanitize(get_post_thumbnail_id(), "medium");
                            ?>
                            <img loading="lazy" class="conversion-image" src="<?= $post_image['src'] ?>" alt="<?= $post_image['alt'] ?>" title="<?= $post_image['title'] ?>" <?= ($post_image['srcset'] ? 'srcset="' . $post_image['srcset'] . '"' : '') ?>>
                        </div>
                        <a href="<?= get_permalink() ?>" alt="<?= get_the_title() ?>" class="journal-name"><?= get_the_title() ?></a>
                        <p class="journal-description"><?= get_the_excerpt() ?></p>
                    </div>
            <?php
                endwhile;
            endif;
            wp_reset_postdata();
            ?>
        </div>
    </section>
<?php endif; ?>