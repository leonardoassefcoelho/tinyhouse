<?php if (is_null(get_field('home-featured-categories-show')) || get_field('home-featured-categories-show')) : ?>
    <section class="home-featured-categories">
        <?php if (!empty(get_field('home-featured-categories-categories'))) : ?>
            <div class="categories-wrapper">
                <?php foreach (get_field('home-featured-categories-categories') as $category) : ?>
                    <div class="category-item">
                        <?php
                        $thumbnail_id = get_term_meta($category->term_id, 'thumbnail_id', true);
                        $categoryImage = tinyhouse_image_sanitize($thumbnail_id, "medium");
                        ?>
                        <div class="img-wrapper">
                            <img loading="lazy" src="<?= $categoryImage['src'] ?>" alt="<?= $categoryImage['alt'] ?>" title="<?= $categoryImage['title'] ?>" <?= ($categoryImage['srcset'] ? 'srcset="' . $categoryImage['srcset'] . '"' : '') ?>>
                        </div>
                        <a href="<?= get_category_link($category->term_id) ?>" alt="<?= $category->name ?>"><?= $category->name ?></a>
                        <p><?= $category->description ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </section>
<?php endif; ?>