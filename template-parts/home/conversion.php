<?php if (is_null(get_field('home-conversion-show')) || get_field('home-conversion-show')) : ?>
    <section class="home-conversion">
        <div class="img-wrapper">
            <?php
            $conversion_image = tinyhouse_image_sanitize(get_field('home-conversion-image'), "full");
            ?>
            <img loading="lazy" class="conversion-image" src="<?= $conversion_image['src'] ?>" alt="<?= $conversion_image['alt'] ?>" title="<?= $conversion_image['title'] ?>" <?= ($conversion_image['srcset'] ? 'srcset="' . $conversion_image['srcset'] . '"' : '') ?>>
        </div>
        <div class="content-wrapper">
            <h2 class="h2 title"><?= get_field('home-conversion-title') ?></h2>
            <p class="subtitle"><?= get_field('home-conversion-subtitle') ?></p>
            <p class="description"><?= get_field('home-conversion-text') ?></p>
            <?php if (is_null(get_field('home-conversion-button-show')) || get_field('home-conversion-button-show')) : ?>
            <a class="button light outlined" target="<?= get_field('home-conversion-button-target') ?>" href="<?= get_field('home-conversion-button-url') ?>" alt="<?= get_field('home-conversion-button-text') ?>"><?= get_field('home-conversion-button-text') ?></a>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>