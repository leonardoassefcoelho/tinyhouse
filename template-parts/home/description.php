<?php if( is_null(get_field('home-description-show')) || get_field('home-description-show') ): ?>
    <section class="home-description padding-container">
        <h2 class="h2"><?= get_field('home-description-title') ?></h2>
        <p><?= get_field('home-description-text') ?></p>
        <?php if( is_null(get_field('home-description-button-show')) || get_field('home-description-button-show') ): ?>
            <a class="button dark outlined" href="<?= get_field('home-description-button-url') ?>" alt="<?= get_field('home-description-button-text') ?>"><?= get_field('home-description-button-text') ?></a>
        <?php endif; ?>
    </section>
<?php endif; ?>