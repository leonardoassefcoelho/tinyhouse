<?php if (is_null(get_field('home-highlights-show')) || get_field('home-highlights-show')) : ?>
    <section class="home-highlights <?= (is_null(get_field('home-highlights-animated')) || get_field('home-highlights-animated')) ? 'animated' : '' ?>">
        <?php for ($i = 1; $i <= 2; $i++) : ?>
            <p>
                <?php
                if (have_rows('home-highlights-repeater')) :
                    while (have_rows('home-highlights-repeater')) : the_row();
                ?>
                        <span><?= get_sub_field('home-highlights-text') ?></span>
                <?php
                    endwhile;
                endif;
                ?>
            </p>
        <?php endfor; ?>
    </section>
<?php endif; ?>