<section class="home-hero <?= get_field("home-hero-autoplay") ? "autoplay" : "" ?>" data-autoplay="<?= get_field("home-hero-autoplay-delay") ?>">
    <?php
        if(have_rows('home-hero-repeater')):
            while(have_rows('home-hero-repeater')) : the_row();
                $hero_image = tinyhouse_image_sanitize(get_sub_field('home-hero-image'), "full");
    ?>
        <div class="hero-slide">
            <img loading="lazy" class="hero-image" src="<?=$hero_image['src']?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?> >
            <div class="content-wrapper">
                <h2 class="h1 hero-title"><?= get_sub_field('home-hero-title') ?></h2>
                <p class="hero-description"><?= get_sub_field('home-hero-description') ?></p>
                <a class="button light default" href="<?= get_sub_field('home-hero-button-url') ?>" alt="<?= get_sub_field('home-hero-button-text') ?>"><?= get_sub_field('home-hero-button-text') ?></a>
            </div>
        </div>
    <?php
            endwhile;
        endif;
    ?>
</section>