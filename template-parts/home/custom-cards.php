<?php if (is_null(get_field('home-custom-card-show')) || get_field('home-custom-card-show')) : ?>
    <section class="home-custom-cards">
        <?php if (have_rows('home-custom-cards-repeater')) :
            while (have_rows('home-custom-cards-repeater')) : the_row(); ?>
                <div class="custom-item">
                    <?php $card_image = tinyhouse_image_sanitize(get_sub_field('home-custom-cards-image'), "medium");
                    ?>
                    <div class="img-wrapper">
                        <img loading="lazy" src="<?= $card_image['src'] ?>" alt="<?= $card_image['alt'] ?>" title="<?= $card_image['title'] ?>" <?= ($card_image['srcset'] ? 'srcset="' . $card_image['srcset'] . '"' : '') ?>>
                    </div>
                    <div class="content-wrapper">
                        <h2 class="h2 title"><?= get_sub_field('home-custom-cards-title') ?></h2>
                        <a class="button light outlined" target="<?= get_sub_field('home-custom-cards-button-target') ?>" href="<?= get_sub_field('home-custom-cards-button-url') ?>" alt="<?= get_sub_field('home-custom-cards-button-text') ?>"><?= get_sub_field('home-custom-cards-button-text') ?></a>
                    </div>
                </div>
        <?php endwhile;
        endif; ?>
    </section>
<?php endif; ?>