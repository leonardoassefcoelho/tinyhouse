<?php if (is_null(get_field('home-products-show')) || get_field('home-products-show')) : ?>
    <section class="home-products">
        <div class="content-wrapper">
            <h2 class="h2 title"><?= get_field('home-products-title') ?></h2>
            <p class="subtitle"><?= get_field('home-products-subtitle') ?></p>
            <p class="description"><?= get_field('home-products-text') ?></p>
            <?php if (is_null(get_field('home-products-button-show')) || get_field('home-products-button-show')) : ?>
                <a class="button dark outlined" href="<?= get_field('home-products-button-url') ?>" alt="<?= get_field('home-products-button-text') ?>"><?= get_field('home-products-button-text') ?></a>
            <?php endif; ?>
        </div>
        <div class="products-wrapper">
            <?php
            if (have_rows('home-products-repeater')) :
                while (have_rows('home-products-repeater')) : the_row();
                    global $product;
                    $product = wc_get_product(get_sub_field('home-products-product'));
            ?>
                    <?php get_template_part('template-parts/common/product-card', null, $product) ?>
            <?php endwhile;
            endif; ?>
        </div>
    </section>
<?php endif; ?>