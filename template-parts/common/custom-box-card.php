<?php
global $product;
$product_type = $product->get_type();
$data=array();
$data['id']=$product->get_id();
$data['price']=$product->get_price();
$data['title']=$product->get_name();
$data['purchaseable'] = ( $product->is_purchasable() && $product->is_in_stock() ) ? 1 : 0;
$data['qty'] = ( $product->managing_stock() ) ? $product->get_stock_quantity() : null;
if (!$product->is_type('variable') || apply_filters('cpb_product_boxes_allow_variables', false, $product_id)) :
    $data['purchaseable'] = ($product->is_purchasable() && $product->is_in_stock()) ? 1 : 0;
    $out_of_stock = ($data['purchaseable'] === 0) ? true : false;
endif;
?>

<div class="product-item <?= ($product->is_purchasable() && $product->is_in_stock()) ? '' : 'out-of-stock' ?>">
    <div class="img-wrapper">
        <?php
        $product_image = tinyhouse_image_sanitize($args->get_image_id(), "full");
        ?>
        <img loading="lazy" class="conversion-image" src="<?= $product_image['src'] ?>" alt="<?= $product_image['alt'] ?>" title="<?= $product_image['title'] ?>" <?= ($product_image['srcset'] ? 'srcset="' . $product_image['srcset'] . '"' : '') ?>>
    </div>
    <a href="<?= get_permalink($args->get_id()) ?>" alt="<?= $args->get_name() ?>" class="product-name"><?= $args->get_name() ?></a>
    <p class="product-price"><?= wc_price($args->get_price()) ?></p>


    <div class="add-to-cart-wrapper">
        <button data-id="<?= $product->get_id() ?>" data-qty="1" class="add-to-cart button dark default custom-box custom-box-part" <?= ($product->is_purchasable() && $product->is_in_stock()) ? '' : 'disabled' ?>>
            <?= $out_of_stock ? 'Out of Stock' : 'Add to box'; ?>
        </button>
        <input type="hidden" class="cpb_item_meta" value="<?= wc_esc_json(wp_json_encode($data)) ?>">
    </div>
</div>