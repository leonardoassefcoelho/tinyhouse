<?php $post_cat_id = get_the_terms(get_the_ID(), 'product_cat')[0]->term_id; ?>

<nav class="shop-menu padding-container">
    <ul class="menu-wrapper menu-slider">
        <?php
        $index = 0;
        if ($args) :
            foreach ($args as $term) :
                $category = get_term_by("term_taxonomy_id", $term['shop-showcase-category']);
        ?>
                <li class="menu-item <?= !is_shop() && $category->term_id == $post_cat_id ? "active" : "" ?> <?= is_shop() && $index == 0 ? "active" : "" ?> <?= is_shop() ? "anchor" : "default" ?>">
                    <a class="menu-item-url" href="<?= is_shop() ? '#' . $category->slug : get_permalink(wc_get_page_id('shop')) . '#' . $category->slug ?>" alt="<?= $category->name ?>"><?= $category->name ?></a>
                </li>
        <?php
                $index++;
            endforeach;
        endif; ?>
    </ul>
</nav>