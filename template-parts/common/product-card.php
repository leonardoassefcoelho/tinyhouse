<?php
global $product;
?>

<div class="product-item <?= ($product->is_purchasable() && $product->is_in_stock()) ? '' : 'out-of-stock' ?>">
    <a href="<?= get_permalink($args->get_id()) ?>" alt="<?= $args->get_name() ?>" aria-label="<?= $args->get_name() ?>" class="img-wrapper">
    <?php
    $product_image = tinyhouse_image_sanitize($args->get_image_id(), "medium");
    ?>
    <img loading="lazy" class="conversion-image" src="<?= $product_image['src'] ?>" alt="<?= $product_image['alt'] ?>" title="<?= $product_image['title'] ?>" <?= ($product_image['srcset'] ? 'srcset="' . $product_image['srcset'] . '"' : '') ?>>
    </a>
    <a href="<?= get_permalink($args->get_id()) ?>" alt="<?= $args->get_name() ?>" class="product-name"><?= $args->get_name() ?></a>
    <p class="product-price"><?= wc_price($args->get_price()) ?></p>
    <?php get_template_part('template-parts/common/components/counter', '', array(
        'product_id'  => $args->get_id(),
    ));
    ?>
</div>