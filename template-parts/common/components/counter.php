<?php
global $product;

$product_type = $product->get_type();
$product_id = $product->get_id();
$out_of_stock = ($product->is_purchasable() && $product->is_in_stock()) ? false : true;

$min_value = $product->get_min_purchase_quantity();
$max_value = $product->get_max_purchase_quantity();

?>
<div class="add-to-cart-wrapper <?= $product_type == 'variable' ? 'woocommerce-variation-add-to-cart variations_button' : '' ?>">
    <?php
    if ((!$out_of_stock) && $product_type != 'cpb_custom_product_boxes' && ($product_type != 'variable' || !is_shop())) :
    ?>
        <div class="product-counter">
            <button class="sub disabled" aria-label="Subtract">-</button>
            <input type="number" min="<?= esc_attr($min_value); ?>" max="<?= esc_attr(0 < $max_value ? $max_value : ''); ?>" name="quantity" value="1" title="<?php echo esc_attr_x('Qty', 'Product quantity input tooltip', 'woocommerce'); ?>" placeholder="Quantity" inputmode="numeric" />
            <button class="add <?= $max_value == 1 ? "disabled" : "" ?>" aria-label="Add">+</button>
        </div>
    <?php
    endif;
    ?>
    <?php
    if (($product_type == 'variable' || $product_type == 'cpb_custom_product_boxes') && !is_product()) :
    ?>
        <a href="<?= $product->get_permalink() ?>" class="add-to-cart button dark default <?= $product_type == 'cpb_custom_product_boxes' ? 'custom-box' : '' ?>">
            <?= $out_of_stock ? 'Out of Stock' : 'Customize now'; ?>
        </a>
    <?php
    else :
    ?>
        <button data-id="<?= $args['product_id'] ?>" data-qty="1" class="add-to-cart button dark default <?= $product_type == 'cpb_custom_product_boxes' ? 'custom-box' : '' ?>" <?= $product_type == 'cpb_custom_product_boxes' || ($out_of_stock) ? 'disabled' : '' ?>>
            <?= $out_of_stock ? 'Out of Stock' : 'Add to cart'; ?>
        </button>
    <?php
    endif;
    ?>

    <?php if ($product_type == 'cpb_custom_product_boxes') : ?>
        <input type="hidden" id="cpb-box-add-to-cart" class="cpb-box-add-to-cart" name="cpb-box-add-to-cart" value="">
    <?php endif; ?>
</div>