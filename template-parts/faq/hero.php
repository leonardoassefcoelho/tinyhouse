<?php
$pageID = get_the_ID();
?>

<div class="padding-container faq-hero top-header-distance">
    <div class="imgs-wrapper">
        <div class="img-wrapper back-image">
            <?php
            $hero_image = tinyhouse_image_sanitize(get_field('faq-hero-back-img', $pageID), "full");
            ?>
            <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
        </div>
        <div class="img-wrapper front-image">
            <?php
            $hero_image = tinyhouse_image_sanitize(get_field('faq-hero-front-img', $pageID), "full");
            ?>
            <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
        </div>
    </div>

    <div class="hero-content">
        <h1 class="h1 title"><?= get_the_title($pageID) ?></h1>
        <div class="description"><?= get_post_field('post_content', $pageID) ?></div>

        <?php
        if (have_rows('faq-repeater')) :
            $index = 0;
        ?>
            <dl class="faq-wrapper">
                <?php
                while (have_rows('faq-repeater')) : the_row();
                ?>
                    <span itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                        <dt itemprop="name" class="question" aria-expanded="<?= $index ? "false" : "true" ?>" aria-id="<?= $index ?>" role="button" tabindex="0">
                            <?= get_sub_field('faq-question'); ?>
                        </dt>
                        <dd itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer" class="answer" aria-expanded="<?= $index ? "false" : "true" ?>" id="<?= $index ?>">
                            <span itemprop="text"><?= get_sub_field('faq-answer'); ?></span>
                        </dd>
                    </span>
                <?php
                    $index++;
                endwhile;
                ?>
            </dl>
        <?php
        endif;
        ?>
    </div>
</div>