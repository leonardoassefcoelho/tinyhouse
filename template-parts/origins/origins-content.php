<section class="content-wrapper">
    <?php
    $taxonomies = get_terms(array(
        'taxonomy' => 'origins_categories',
        'hide_empty' => true
    ));
    $page_id = get_the_id();
    ?>
    <nav class="origins-menu">
        <ul class="menu-wrapper menu-slider">
            <?php
            if ($taxonomies) :
                foreach ($taxonomies as $index => $category) :
            ?>
                    <li class="menu-item <?= $index == 0 ? "active" : "" ?>">
                        <button class="menu-item-btn" data-origins="<?= $category->slug ?>" aria-label="<?= $category->name ?>"><?= $category->name ?></a>
                    </li>
            <?php
                endforeach;
            endif; ?>
        </ul>
    </nav>
    <div class="origins-wrapper">
        <?php
        if ($taxonomies) :
            foreach ($taxonomies as $index => $category) :
        ?>
                <div class="origin-slider <?= $index == 0 ? "active " .  $category->slug :  $category->slug ?>">
                    <?php
                    $args = array(
                        'post_type' => 'origins',
                        'post_status' => 'publish',
                        'posts_per_page' => 8,
                        'orderby' => 'title',
                        'order' => 'ASC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'origins_categories',
                                'field' => 'term_id',
                                'terms' => $category->term_id,
                            )
                        )
                    );

                    $query = new WP_Query($args);

                    if ($query->have_posts()) :
                        while ($query->have_posts()) : $query->the_post()
                    ?>

                            <div class="origin-item">
                                <?php
                                $image = tinyhouse_image_sanitize(get_post_thumbnail_id(), "full");
                                ?>
                                <div class="img-wrapper">
                                    <img loading="lazy" src="<?= $image['src'] ?>" alt="<?= $image['alt'] ?>" title="<?= $image['title'] ?>" <?= ($image['srcset'] ? 'srcset="' . $image['srcset'] . '"' : '') ?>>
                                </div>
                                <div class="content">
                                    <h2 class="origin-title h2"><?= get_the_title() ?></h2>
                                    <div class="text"><?= the_content(); ?></div>
                                    <div class="conversion-wrapper">
                                        <h3 class="h2"><?= get_field("origins-conversion-text", $page_id) ?></h3>
                                        <a class="button light default" href="<?= get_field("origins-conversion-button-url", $page_id) ?>" aria-label="<?= get_field("origins-conversion-button-text", $page_id) ?>"><?= get_field("origins-conversion-button-text", $page_id) ?></a>
                                    </div>
                                </div>
                            </div>

                    <?php
                        endwhile;
                    endif;
                    wp_reset_postdata();
                    ?>
                </div>
        <?php
            endforeach;
        endif; ?>
    </div>
</section>