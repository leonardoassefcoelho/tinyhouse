<div class="origins-hero">
    <div class="hero-content">
        <?php
        $hero_image = tinyhouse_image_sanitize(get_field('origins-hero-image'), "full");
        ?>
        <div class="img-wrapper">
            <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
        </div>
        <div class="text-wrapper-block-one show">
            <?php
            $origins_hero_logo = get_field('origins-hero-logo');
            if ($origins_hero_logo) :
                echo file_get_contents($origins_hero_logo);
            endif;
            ?>
            <h1 class="h1 title"><?= get_the_title(); ?></h1>
        </div>
        <div class="text-wrapper-block-two">
            <h2 class="h1 description"><?= get_field('origins-hero-text') ?></h2>
            <?php if (is_null(get_field('origins-hero-btn-show')) || get_field('origins-hero-btn-show')) : ?>
                <a class="button default light" target="<?= get_field('origins-hero-btn-target') ?>" href="<?= get_field('origins-hero-btn-url') ?>" alt="<?= get_field('origins-hero-btn-text') ?>"><?= get_field('origins-hero-btn-text') ?></a>
            <?php endif; ?>
        </div>
    </div>
</div>