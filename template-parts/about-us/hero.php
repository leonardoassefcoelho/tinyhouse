<?php
$pageID = get_the_ID();
?>

<div class="padding-container about-us-hero top-header-distance">
    <div class="imgs-wrapper">
        <div class="img-wrapper back-image">
            <?php
            $hero_image = tinyhouse_image_sanitize(get_field('about-us-hero-back-img', $pageID), "full");
            ?>
            <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
        </div>
        <div class="img-wrapper front-image">
            <?php
            $hero_image = tinyhouse_image_sanitize(get_field('about-us-hero-front-img', $pageID), "full");
            ?>
            <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
        </div>
    </div>

    <div class="hero-content">
        <h1 class="h1 title"><?= get_the_title($pageID) ?></h1>
        <div class="description"><?= get_post_field('post_content', $pageID) ?></div>
    </div>

    <div class="img-wrapper first-image">
        <?php
        $hero_image = tinyhouse_image_sanitize(get_field('about-us-hero-first-img', $pageID), "full");
        ?>
        <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
    </div>

    <div class="img-wrapper second-image">
        <?php
        $hero_image = tinyhouse_image_sanitize(get_field('about-us-hero-second-img', $pageID), "full");
        ?>

        <?php
        $hero_badge = get_field('about-us-hero-badge');
        if ($hero_badge) :
            echo file_get_contents($hero_badge);
        endif;
        ?>
        <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
    </div>
</div>