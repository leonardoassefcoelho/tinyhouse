<?php
if (have_rows('about-us-repeater')) :
    $content = get_field('about-us-repeater');
?>
    <nav class="about-menu padding-container">
        <ul>
            <?php
            foreach ($content as $index => $section) :
            ?>
                <li class="menu-item <?= !$index ? "active" : "" ?>">
                    <a href="#<?= sanitize_title($section["about-us-title"]) ?>"><?= $section['about-us-title'] ?></a>
                </li>

            <?php
            endforeach;
            ?>
        </ul>
    </nav>

    <div class="about-us-wrapper padding-container">
        <?php
        foreach ($content as $section) :
        ?>
            <section class="section" id="<?= sanitize_title($section["about-us-title"]) ?>">
                <div class="imgs-wrapper">
                    <div class="img-wrapper back-image">
                        <?php
                        $hero_image = tinyhouse_image_sanitize($section['about-us-back-image'], "full");
                        ?>
                        <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
                    </div>
                    <div class="img-wrapper front-image">
                        <?php
                        $hero_image = tinyhouse_image_sanitize($section['about-us-front-image'], "full");
                        ?>
                        <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>
                    </div>
                </div>

                <div class="hero-content">
                    <h1 class="h1 title"><?= $section["about-us-title"] ?></h1>
                    <div class="description"><?= $section["about-us-content"] ?></div>
                </div>
            </section>
        <?php
        endforeach;
        ?>
    </div>
<?php
endif;
?>