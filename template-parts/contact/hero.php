<div class="contact-hero">
    <?php
    $hero_image = tinyhouse_image_sanitize(get_field('contact-hero-image'), "full");
    ?>
    <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>

    <div class="hero-content">
        <?php
        $contact_hero_logo = get_field('contact-hero-logo');
        if ($contact_hero_logo) :
            echo file_get_contents($contact_hero_logo);
        endif; ?>
        <h2 class="h1 title"><?= get_field('contact-hero-text') ?></h2>
    </div>
</div>