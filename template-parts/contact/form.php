<div class="form-wrapper padding-container">
    <div class="notices">
        <?php
        if (have_rows('contact-notices-repeater')) :
            while (have_rows('contact-notices-repeater')) : the_row();
        ?>
                <div class="notice">
                    <h3 class="title"><?= get_sub_field("contact-notice-title") ?></h3>
                    <p class="text"><?= get_sub_field("contact-notice-text") ?></p>
                </div>
        <?php
            endwhile;
        endif;
        ?>
    </div>

    <div class="contact-form-wrapper">
        <h2 class="form-title h2"><?= get_field('contact-form-title') ?></h2>
        <form class="contact-form" action="<?= admin_url('admin-ajax.php') ?>?action=tinyhouse_sendmail" method="post">
            <p class="description"><?= get_field('contact-form-description') ?></p>

            <?php
            wp_nonce_field("contact_form", "contact-nonce");
            ?>

            <input class="input" required id="name" name="name" placeholder="Name" type="text" aria-label="Name">
            <input class="input" required id="email" name="email" placeholder="Email address" type="email" aria-label="Email address">
            <input class="input" required id="subject" name="subject" placeholder="Subject" type="text" aria-label="Subject">

            <textarea id="message" required name="message" placeholder="Message" aria-label="Message"></textarea>

            <button class="button dark default" type="submit" aria-label="<?= get_field('contact-form-button-text') ?>"><?= get_field('contact-form-button-text') ?></button>
        </form>
        <div class="img-wrapper">
            <?php
            $contact_image = tinyhouse_image_sanitize(get_field('contact-form-image'), "full");
            ?>
            <img loading="lazy" class="hero-image" src="<?= $contact_image['src'] ?>" alt="<?= $contact_image['alt'] ?>" title="<?= $contact_image['title'] ?>" <?= ($contact_image['srcset'] ? 'srcset="' . $contact_image['srcset'] . '"' : '') ?>>
        </div>
    </div>
</div>