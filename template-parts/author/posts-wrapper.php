<?php
if (have_posts()) :
?>
    <div class="posts-wrapper">
        <h2 class="h2 title">Articles By <?= get_the_author_meta("display_name") ?></h2>
        <div class="posts-container padding-container">
            <div class="slider-wrapper">
                <?php
                while (have_posts()) : the_post();
                ?>
                    <div class="post-card">
                        <div class="img-wrapper">
                            <?php
                            $thumbnail = tinyhouse_image_sanitize(get_post_thumbnail_id(), "full");
                            ?>
                            <img loading="lazy" class="hero-image" src="<?= $thumbnail['src'] ?>" alt="<?= $thumbnail['alt'] ?>" title="<?= $thumbnail['title'] ?>" <?= ($thumbnail['srcset'] ? 'srcset="' . $thumbnail['srcset'] . '"' : '') ?>>
                        </div>
                        <div class="content-wrapper">
                            <a class="title" aria-label="<?= get_the_title() ?>" href="<?= get_permalink() ?>"><?= get_the_title() ?></a>
                            <p class="excerpt"><?= get_the_excerpt() ?></p>
                        </div>

                    </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </div>
<?php
endif;
?>