<?php $template_directory_uri = get_template_directory_uri(); ?>
<div class="hero padding-container top-header-distance">
    <div class="img-wrapper">
        <?= get_avatar(get_the_author_meta("ID"), 460) ?>
    </div>
    <div class="content-wrapper">
        <h1 class="h1 title"><?= get_the_author_meta("display_name") ?></h1>
        <p class="description"><?= get_the_author_meta("user_description") ?></p>

        <div class="social-wrapper">
            <?php if (get_the_author_meta("linkedin")) : ?>
                <a href="<?= get_the_author_meta("linkedin") ?>" rel="nofollow" target="_blank" aria-label="Linkedin">
                    <?= file_get_contents($template_directory_uri . "/assets/icons/linkedin.svg") ?>
                </a>
            <?php endif; ?>
            <?php if (get_the_author_meta("facebook")) : ?>
                <a href="<?= get_the_author_meta("facebook") ?>" rel="nofollow" target="_blank" aria-label="Facebook">
                    <?= file_get_contents($template_directory_uri . "/assets/icons/facebook.svg") ?>
                </a>
            <?php endif; ?>
            <?php if (get_the_author_meta("instagram")) : ?>
                <a href="<?= get_the_author_meta("instagram") ?>" rel="nofollow" target="_blank" aria-label="Instagram">
                    <?= file_get_contents($template_directory_uri . "/assets/icons/instagram.svg") ?>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>