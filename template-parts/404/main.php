<div class="not-found">
    <div class="img-wrapper">
        <?php
        $background = tinyhouse_image_sanitize(get_field('404-background-img', "option"), "full");
        ?>
        <img loading="lazy" class="hero-image" src="<?= $background['src'] ?>" alt="<?= $background['alt'] ?>" title="<?= $background['title'] ?>" <?= ($background['srcset'] ? 'srcset="' . $background['srcset'] . '"' : '') ?>>
    </div>
    <div class="content">
        <h1 class="h1">404</h1>
        <p class="description"><?= get_field("404-description", "option") ?></p>

        <a class="button light default" href="<?= get_field('404-btn-url', "option") ?>" alt="<?= get_field('404-btn-text', "option") ?>"><?= get_field('404-btn-text', "option") ?></a>
    </div>
</div>