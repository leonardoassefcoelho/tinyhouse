<div class="shop-menu padding-container">
    <ul class="menu-wrapper menu-slider">
        <?php
        $index = 0;
        if ($args) :
            foreach ($args as $term) :
                $category = get_term_by("term_taxonomy_id", $term['shop-showcase-category']);
        ?>
                <li class="menu-item <?= $index == 0 ? "active" : "" ?>">
                    <a class="menu-item-url" href="#<?= $category->slug ?>" alt="<?= $category->name ?>"><?= $category->name ?></a>
                </li>
        <?php
                $index++;
            endforeach;
        endif; ?>
    </ul>
</div>