<div class="shop-hero">
    <?php
    $hero_image = tinyhouse_image_sanitize(get_field('shop-hero-image', get_option('woocommerce_shop_page_id')), "full");
    ?>
    <img loading="lazy" class="hero-image" src="<?= $hero_image['src'] ?>" alt="<?= $hero_image['alt'] ?>" title="<?= $hero_image['title'] ?>" <?= ($hero_image['srcset'] ? 'srcset="' . $hero_image['srcset'] . '"' : '') ?>>

    <div class="hero-content">
        <?php
        $shop_hero_logo = get_field('shop-hero-logo', get_option('woocommerce_shop_page_id'));
        if ($shop_hero_logo) :
            echo file_get_contents($shop_hero_logo);
        endif;
        ?>
        <h1 class="h1 title"><?= get_field('shop-hero-text', get_option('woocommerce_shop_page_id')) ?></h1>
    </div>
</div>