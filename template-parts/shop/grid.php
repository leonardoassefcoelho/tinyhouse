<div class="shop-grid">
    <?php
    if ($args) :
        foreach ($args as $term) :
            $category = get_term_by("term_taxonomy_id", $term['shop-showcase-category']);
            if ($category->count > 0) :
                $args = array(
                    'post_type'      => 'product',
                    'posts_per_page' => -1,
                    'ignore_sticky_posts' => true,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field' => 'term_id',
                            'terms' => $category->term_id
                        ),
                    ),
                );
                if (is_product_tag()) :
                    $tag = get_queried_object();
                    $tag_object = get_tag($tag);
                    $args['tax_query'][] = array(
                        'taxonomy' => 'product_tag',
                        'terms'    => $tag_object->slug,
                        'field'    => 'slug',
                    );
                endif;

                $order = get_field("product-cat-order", 'term_' . $category->term_id);
                $order_by = get_field("product-cat-order-by", 'term_' . $category->term_id);
                $prioritize_stock = get_field("product-cat-prioritize-stock", 'term_' . $category->term_id);
                $show_out_of_stock = get_field("product-cat-show-out-of-stock", 'term_' . $category->term_id);

                if ($order) :
                    switch ($order):
                        case "stock":
                            $args['meta_key'] = '_stock';
                            $args['orderby'] = 'meta_value_num';
                            break;
                        case "popularity":
                            $args['meta_key'] = 'post_view_count';
                            $args['orderby'] = 'meta_value_num';
                            break;
                        case "title":
                            $args['orderby'] = "title";
                            break;
                        case "date":
                            $args['orderby'] = "date";
                            break;
                        case "price":
                            $args['meta_key'] = '_price';
                            $args['orderby'] = 'meta_value_num';
                            break;
                    endswitch;
                else :
                    $args['meta_key'] = '_stock';
                    $args['orderby'] = 'meta_value_num';
                endif;

                if ($order_by) :
                    $args['order'] = $order_by;
                endif;

                if (is_null($show_out_of_stock) || $show_out_of_stock) :
                    if (is_null($prioritize_stock) || $prioritize_stock) :
                        $args_out_of_stock = $args;
                        $args_out_of_stock['meta_query'] = array(
                            array(
                                'key' => '_stock_status',
                                'value' => 'outofstock',
                                'compare' => '=',
                            )
                        );

                        $products_out_of_stock = new WP_Query($args_out_of_stock);

                    endif;
                    $args['meta_query'] = array(
                        array(
                            'key' => '_stock_status',
                            'value' => 'instock',
                            'compare' => '=',
                        )
                    );
                endif;

                $products = new WP_Query($args);

                if ($products->have_posts()) :
    ?>
                    <div class="product-section padding-container" id="<?= $category->slug ?>">
                        <h2 class="section-title h2">
                            <?= $category->name ?>
                        </h2>
                        <p class="description">
                            <?= $category->description ?>
                        </p>

                        <div class="products-wrapper">
                            <?php
                            while ($products->have_posts()) : $products->the_post();
                                global $product;
                                get_template_part('template-parts/common/product-card', null, $product);
                            endwhile;
                            wp_reset_query();

                            if (isset($products_out_of_stock)) {
                                while ($products_out_of_stock->have_posts()) : $products_out_of_stock->the_post();
                                    global $product;
                                    get_template_part('template-parts/common/product-card', null, $product);
                                endwhile;
                                wp_reset_query();
                            }
                            if ($term['shop-showcase-feature-box-show']) :
                                get_template_part('template-parts/shop/featured-box', null, $term);
                            endif;
                            ?>
                        </div>
                    </div>
    <?php
                endif;
                wp_reset_postdata();
            endif;
        endforeach;
    endif; ?>
</div>