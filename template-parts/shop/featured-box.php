<div class="product-item featured-box">
    <div class="wrapper">
        <?php
        $featured_image = tinyhouse_image_sanitize($args['shop-showcase-feature-box-image'], "full");
        ?>
        <div class="img-wrapper">
            <img loading="lazy" class="conversion-image" src="<?= $featured_image['src'] ?>" alt="<?= $featured_image['alt'] ?>" title="<?= $featured_image['title'] ?>" <?= ($featured_image['srcset'] ? 'srcset="' . $featured_image['srcset'] . '"' : '') ?>>
        </div>
        <div class="content-wrapper">
            <h3 class="title h2"><?= $args['shop-showcase-feature-box-title'] ?></h3>
            <p class="description"><?= $args['shop-showcase-feature-box-text'] ?></p>
            <?php if ($args['shop-showcase-feature-box-button-show']) : ?>
                <a class="button light outlined" target="<?= $args['shop-showcase-feature-box-button-target'] ?>" href="<?= $args['shop-showcase-feature-box-button-url'] ?>" alt="<?= $args['shop-showcase-feature-box-button-text'] ?>"><?= $args['shop-showcase-feature-box-button-text'] ?></a>
            <?php endif; ?>
        </div>
    </div>
</div>