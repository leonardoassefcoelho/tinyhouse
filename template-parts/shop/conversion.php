<section class="conversion-section padding-container">
    <?php
    $conversion_image = tinyhouse_image_sanitize(get_field('shop-conversion-background', get_option('woocommerce_shop_page_id')), "full");
    ?>
    <div class="img-wrapper">
        <img loading="lazy" class="conversion-image" src="<?= $conversion_image['src'] ?>" alt="Background" title="Background" <?= ($conversion_image['srcset'] ? 'srcset="' . $conversion_image['srcset'] . '"' : '') ?>>
    </div>
    <div class="content-wrapper padding-container">
        <h2 class="h2"><?= get_field('shop-conversion-title', get_option('woocommerce_shop_page_id')) ?></h2>
        <p class="subtitle"><?= get_field('shop-conversion-subtitle', get_option('woocommerce_shop_page_id')) ?></p>
        <p class="description"><?= get_field('shop-conversion-description', get_option('woocommerce_shop_page_id')) ?></p>
        <a class="button light outlined" href="<?= get_field('shop-conversion-btn-url', get_option('woocommerce_shop_page_id')) ?>" alt="<?= get_field('shop-conversion-btn-text', get_option('woocommerce_shop_page_id')) ?>"><?= get_field('shop-conversion-btn-text', get_option('woocommerce_shop_page_id')) ?></a>
    </div>
</section>