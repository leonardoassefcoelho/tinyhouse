<div class="privacy-wrapper padding-container">
    <h1 class="h1 title"><?= get_the_title() ?></h1>
    <div class="content">
        <?= the_content() ?>
    </div>
</div>