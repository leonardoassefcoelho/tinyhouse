<?php
/* Template Name: Page Checkout */
get_header();
$breadcrumb_args = array(
    'wrap_before' => '<nav class="woocommerce-breadcrumb">',
    'wrap_after' => '</nav>',
    'delimiter' => '<span> > </span>'
);
?>

<div class="checkout-content">
    <?php get_template_part('template-parts/checkout/hero');  ?>
    <div class="padding-container">
        <?php woocommerce_breadcrumb($breadcrumb_args); ?>
        <h1 class="title h1"><?= !empty( is_wc_endpoint_url('order-received')) ? "Thank you" : get_the_title() ?></h1>
        <?= the_content(); ?>
    </div>
</div>

<?php
get_footer();
