<?php

require_once 'inc/setup.php';
require_once 'inc/ajax.php';
require_once 'inc/utils/utils.php';
require_once 'inc/custom-post-types/origins.php';
require_once 'inc/custom-post-types/radios.php';
require_once 'inc/custom-taxonomies/origins_categories.php';
require_once 'inc/custom-taxonomies/product_labels.php';
