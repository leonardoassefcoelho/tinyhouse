<?php
/* Template Name: Page Radios */
get_header();
?>

<div class="blog-content">
    <?php get_template_part('template-parts/radios/hero');  ?>
    <?php get_template_part('template-parts/radios/posts-wrapper');  ?>
</div>

<?php
get_footer();
