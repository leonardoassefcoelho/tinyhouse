<?php
get_header();
$template_directory_uri = get_template_directory_uri();
?>
<div class="single">
    <article class="padding-container">
        <div class="hero">
            <h1 class="title"><?= get_the_title() ?></h1>

            <p class="excerpt"><?= get_field("single-post-subtitle") ?></p>

            <p class="infos"><time pubdate><?= get_the_date("F d, Y") ?></time> | Written by: <a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a></p>
        </div>

        <div class="img-wrapper <?= is_singular("radios") ? "square" : "" ?>">
            <?php
            $thumbnail = tinyhouse_image_sanitize(get_post_thumbnail_id(), "full");
            ?>
            <img loading="lazy" class="hero-image" src="<?= $thumbnail['src'] ?>" alt="<?= $thumbnail['alt'] ?>" title="<?= $thumbnail['title'] ?>" <?= ($thumbnail['srcset'] ? 'srcset="' . $thumbnail['srcset'] . '"' : '') ?>>
        </div>
        <div class="content">
            <?php the_content() ?>
        </div>
    </article>
    <div class="share-wrapper padding-container">
        <p class="title">Share with your friends</p>
        <div class="share-buttons-wrapper">
            <a class="share facebook" rel="nofollow" href="http://www.facebook.com/share.php?u=<?= get_the_permalink() ?>" target="_blank" aria-label="Share on facebook">
                <?= file_get_contents($template_directory_uri . "/assets/icons/facebook.svg") ?>
            </a>
            <span class="share copy" data-clipboard-text="<?= get_the_permalink() ?>">
                <?= file_get_contents($template_directory_uri . "/assets/icons/copy.svg") ?>
            </span>
        </div>
    </div>
    <div class="author-wrapper container">
        <div class="img-wrapper">
            <?=
            get_avatar(get_the_author_meta('ID'))
            ?>
        </div>
        <div class="author-infos">
            <p class="name">Written by: <a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a></p>
            <p class="description"><?= the_author_meta('description', get_the_author_meta('ID')) ?></p>
        </div>
    </div>
    <?php
    $args = array(
        'post_type' => get_post_type(),
        'post_status' => 'publish',
        'posts_per_page' => 3,
        'orderby' => 'rand',
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) :
    ?>
        <div class="more-posts padding-container">
            <h2 class="title"><?= is_singular("radios") ? "More radios" : "More posts" ?></h2>
            <div class="slider-wrapper">
                <?php while ($query->have_posts()) : $query->the_post() ?>
                    <div class="post-card <?= is_singular("radios") ? "radio" : "" ?>">
                        <div class="img-wrapper">
                            <?php
                            $post_image = tinyhouse_image_sanitize(get_post_thumbnail_id(), "full");
                            ?>
                            <img loading="lazy" class="conversion-image" src="<?= $post_image['src'] ?>" alt="<?= $post_image['alt'] ?>" title="<?= $post_image['title'] ?>" <?= ($post_image['srcset'] ? 'srcset="' . $post_image['srcset'] . '"' : '') ?>>
                        </div>
                        <a href="<?= get_permalink() ?>" alt="<?= get_the_title() ?>" class="title"><?= get_the_title() ?></a>

                        <?php if (!is_singular("radios")) : ?>
                            <p class="description"><?= get_the_excerpt() ?></p>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    <?php
    endif;
    wp_reset_postdata();
    ?>
    <?php
    if (comments_open()) :
    ?>
        <div class="comments-wrapper padding-container">
            <h2 class="title">Comments</h2>
            <?php
            if (get_comments_number()) :
                comments_template();
            endif;
            ?>
        </div>
    <?php
    endif;
    ?>
</div>
<?php
get_footer();
