<?php
get_header();
?>

<div class="blog-content">

    <?php get_template_part('template-parts/journals/hero');  ?>
    <?php get_template_part('template-parts/journals/posts-wrapper');  ?>

</div>

<?php 
get_footer();
