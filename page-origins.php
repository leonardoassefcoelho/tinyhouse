<?php
/* Template Name: Page Origins */
get_header();
?>

    <?php get_template_part('template-parts/origins/hero'); ?>
    <?php get_template_part('template-parts/origins/origins-content'); ?>

<?php
get_footer();
