<?php
/* Template Name: Page About Us */
get_header();

?>

<div class="about-us-content">
    <?php get_template_part('template-parts/about-us/hero');  ?>
    <?php get_template_part('template-parts/about-us/about-content');  ?>
</div>

<?php
get_footer();
