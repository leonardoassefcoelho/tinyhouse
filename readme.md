# Tinyhouse Chocolate Wordpress Theme

## Dependencies
- Wordpress 5.7.1+
- Advanced Custom Fields Pro 5.11.1+
- WooCommerce 6.0.0+
- Custom Product Boxes 1.0.4+

## Technologies
- [PHP](https://www.php.net) 7.4
- [MySql](https://www.mysql.com) 8+
- [jQuery](https://jquery.com)
- [Nginx](https://www.nginx.com) or [Apache](https://www.apache.org) HTTP Server
- [node.js](https://nodejs.org/en/) 17.2+

## Authomation
- [NPM](https://www.npmjs.com)
- [Gulp](https://gulpjs.com)  2.3.0+
- [Babel](https://babeljs.io)
- [SCSS](https://sass-lang.com)
- [CSS Nano](https://cssnano.co)
- [Autoprefixer](https://github.com/postcss/autoprefixer)

## Set Up - Development
CSS and JavaScript must be written/edited in sass and js-dev folders. After running the gulp script it will look for changes and generate optimized and minified css and javascript outputs at css and js folders. 
Don't EVER make changes to the output folders.

Inside the theme directory install NPM modules:
```sh
npm install
```
Then, run gulp:
```sh
gulp
```

## Good to know
- Css and Js are generated individually for each page to avoid unused code.
- Each page css is located on the root of the sass folder and imports components from the children folders.
- Js files are separeted by folders for each page and files for each componennt/section.
- Those files are individually enqueued at inc/setup.php file.
- Every ACF Pro field is saved in the acf-json folder at the root of the theme, grant read/write (755) permissions to this folder to keep the field syncing. After updating field locally and uploading to the server go to **Custom Fields -> Fields Groups -> Sync and import the new fields**.

## Developer and support
[Leonardo Assef Coelho](https://www.linkedin.com/in/leonardo-assef-coelho-51b63b138/)