<?php
/**
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.7.0
 */

$terms = have_rows('shop-showcase-categories-order', get_option('woocommerce_shop_page_id')) ? get_field('shop-showcase-categories-order', get_option('woocommerce_shop_page_id')) : null;
$breadcrumb_args = array(
	'wrap_before' => '<nav class="woocommerce-breadcrumb padding-container mobile">',
	'wrap_after' => '</nav>',
	'delimiter' => '<span> > </span>'
);
get_header('shop');
?>

<div <?php wc_product_class('shop-content top-header-distance', $product); ?>>
	<?php while (have_posts()) : the_post(); ?>
		<?php
		get_template_part('template-parts/common/menu-shop', null, $terms);
		woocommerce_breadcrumb($breadcrumb_args);
		?>

		<section class="product-details padding-container" itemscope itemtype="https://schema.org/Product">
			<?php
			if (empty($product) || !$product->is_visible()) :
				return;
			//Atualizar com uma msg de erro!!!
			else :
				get_template_part('template-parts/single-product/gallery');
				get_template_part('template-parts/single-product/product-infos');
			endif;
			?>

		</section>
		<?php get_template_part('template-parts/single-product/featured-box'); ?>
		<?php get_template_part('template-parts/single-product/related-products'); ?>
	<?php endwhile; ?>
</div>

<?php
get_footer('shop');
