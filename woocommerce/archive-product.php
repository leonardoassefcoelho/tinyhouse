<?php
/**
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.7.0
 */
$terms = have_rows('shop-showcase-categories-order', get_option('woocommerce_shop_page_id')) ? get_field('shop-showcase-categories-order', get_option('woocommerce_shop_page_id')) : null;

get_header('shop');
?>


<div class="shop-content">
	
	<?php
	get_template_part('template-parts/shop/hero');
	get_template_part('template-parts/common/menu-shop', null, $terms);
	get_template_part('template-parts/shop/grid', null, $terms);
	get_template_part('template-parts/shop/conversion', null, $terms);
	?>

</div>

<?php
get_footer('shop');
?>