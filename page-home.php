<?php
/* Template Name: Page Home */
get_header();
?>

<div class="home-content">
    <?php get_template_part( 'template-parts/home/hero' ); ?>
    <?php get_template_part( 'template-parts/home/description' ); ?>
    <?php get_template_part( 'template-parts/home/highlights' ); ?>
    <?php get_template_part( 'template-parts/home/featured-categories' ); ?>
    <?php get_template_part( 'template-parts/home/conversion' ); ?>
    <?php get_template_part( 'template-parts/home/products' ); ?>
    <?php get_template_part( 'template-parts/home/badges' ); ?>
    <?php get_template_part( 'template-parts/home/custom-cards' ); ?>
    <?php get_template_part( 'template-parts/home/journals' ); ?>
</div>

<?php
get_footer();
