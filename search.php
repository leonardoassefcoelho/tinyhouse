<?php
/* Template Name: Page FAQ */
get_header();
?>

<div class="search-content padding-container top-header-distance">
    <form role="search" method="get" id="searchform" class="searchform" action="<?= esc_url(home_url('/')); ?>">
        <label class="screen-reader-text" for="s">Search for:</label>
        <input type="text" placeholder="Search for categories, products, materials or anything else" value="<?= $_GET['s'] ?>" name="s" id="s">
        <button type="submit" aria-label="Search">
            <?= file_get_contents(get_template_directory_uri() . "/assets/icons/search.svg") ?>
        </button>
    </form>
    <?php
    $args = array(
        's' => $_GET['s'],
        'post_type' => "product"
    );
    $query = new WP_Query($args);

    $result = $query->found_posts < 2 ? " result" : " results";
    ?>

    <p class="search-results">Showing <?= $query->found_posts . $result ?></p>

    <?php

    if ($query->have_posts()) :
    ?>
    <div class="shop-grid">
        <div class="product-section padding-container">
            <div class="products-wrapper">
                <?php
                while ($query->have_posts()) : $query->the_post();
                    global $product;
                    get_template_part('template-parts/common/product-card', null, $product);
                endwhile;
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
    <?php
    endif;
    wp_reset_postdata();
    ?>

</div>

<?php
get_footer();
