var target = window.location.hash,
  target = target.replace("#", "");

window.location.hash = "";

$(document).ready(function () {
  if (target) {
    var sticky = $(".shop-menu"),
      header = $(".header");

    $("html, body").animate(
      {
        scrollTop: $("#" + target).offset().top - sticky.height() - header.height(),
      },
      "slow"
    );

    if ($(".shop-menu .menu-item").hasClass("anchor")) {
      $(".shop-menu .menu-item.active").removeClass("active");
      $(".shop-menu .menu-item .menu-item-url[href*='#" + target + "']").parent().addClass("active");
    }
  }

  if (window.location.hash) {
    $(".shop-menu .menu-item.active").removeClass("active");

    $(".shop-menu .menu-item a[href='" + window.location.hash + "']")
      .parent()
      .addClass("active");
  }

  var stickyOffset = $(".shop-menu").offset().top;

  $(window).scroll(function () {
    stickShopMenu(stickyOffset);
  });

  stickShopMenu(stickyOffset);

  $(".shop-menu .menu-slider").slick({
    arrows: false,
    mobileFirst: true,
    slidesToShow: 1.5,
    slidesToScroll: 1,
    infinite: false,
    focusOnSelect: true,
    accessibility: true,
    touchThreshold: 100,
    responsive: [
      {
        breakpoint: 399,
        settings: {
          slidesToShow: 2.5,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 699,
        settings: {
          slidesToShow: 3.5,
          slidesToScroll: 3,
        },
      }
    ],
  });

  $(".shop-menu .menu-item").on("click", function (e) {
    if ($(this).hasClass("anchor")) {
      $(".shop-menu .menu-item.active").removeClass("active");
      $(this).addClass("active");

      var sticky = $(".shop-menu"),
        header = $(".header");

      e.preventDefault();

      var section = $(this).children(".menu-item-url").attr("href");

      $("html,body").animate(
        {
          scrollTop:
            $(section).offset().top - sticky.height() - header.height(),
        },
        "slow"
      );
    }
  });
});

function stickShopMenu(stickyOffset) {
  var sticky = $(".shop-menu"),
    scroll = $(window).scrollTop();

  if (scroll >= stickyOffset - sticky.height()) {
    sticky.addClass("fixed");
    $(".shop-grid").css("margin-top", sticky.height());
  } else {
    sticky.removeClass("fixed");
    $(".shop-grid").css("margin-top", 0);
  }
}
