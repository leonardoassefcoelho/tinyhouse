$(document).ready(function () {
  addToCartModule();
  updateCart();

  $("button.cart").on("click", function () {
    toggleSideCart();
  });

  $(".sidecart button.close-btn").on("click", function () {
    toggleSideCart();
  });

  $(".sidecart").click(function (e) {
    if (e.target == this) {
      toggleSideCart();
    }
  });

  $(".sidecart").on("click", ".remove", function () {
    const url = $(this).data("url");

    removeItem(url);
  });
});

function toggleSideCart() {
  $(".sidecart").toggleClass("active");
}

function updateCartTotal(animate = false, updateCart = true) {
  var settings = {
    url: "/wp-admin/admin-ajax.php?action=cart_total_items",
    method: "GET",
    timeout: 0,
  };

  $.ajax(settings).done(function (response) {
    if (updateCart) {
      updateSidecart();
    }

    if (parseInt(response) > 99) {
      $(".header .cart .cart-count").text("99+");
      $(".header .cart .cart-count").addClass("smaller");
    } else {
      $(".header .cart .cart-count").text(parseInt(response));
      if ($(".header .cart .cart-count").hasClass("smaller")) {
        $(".header .cart .cart-count").removeClass("smaller");
      }
    }

    if (animate) {
      $(".header .cart").addClass("animate");
      setTimeout(function () {
        $(".header .cart").removeClass("animate");
      }, 820);
    }
  });
}

function updateSidecart() {
  $(".sidecart .wrapper").append("<div class='loading'></div>");

  var settings = {
    url: "/wp-admin/admin-ajax.php?action=update_sidecart",
    method: "GET",
    timeout: 0,
  };

  $.ajax(settings).done(function (response) {
    const cart = $.parseHTML(response.data.cart);
    const subtotal = $.parseHTML(response.data.subtotal);

    if (response && cart != "") {
      $(".sidecart .empty-cart").remove();
      $(".sidecart .shop_table tbody").empty().append(cart);

      //Hotfix para Object Fit em imagens carregadas via AJAX para o Safari
      if (navigator.userAgent.indexOf("Safari") != -1) {
        const img = $(".sidecart .cart_item .product-thumbnail img");
        const src = $(img).attr("src");
        $(img).attr("src", src);
      }
    } else {
      const emptyHtml =
        '<div class="empty-cart"><p class="notice">Your cart is empty</p><p class="text">Return to home page or choose other products</p></div>';

      $(".sidecart .shop_table tbody").empty();
      $(".sidecart .shop_table").before(emptyHtml);
    }

    $(".sidecart p[data-title='Subtotal']").empty().append(subtotal);
    $(".sidecart .wrapper .loading").remove();
    updateCartTotal(false, false);
  });
}

function removeItem(url) {
  var settings = {
    url: url,
    method: "GET",
    timeout: 0,
  };

  $.ajax(settings).done(function () {
    updateSidecart();
  });
}

function addToCartModule() {
  $(document).on("click", "button.sub", function (event) {
    event.preventDefault();

    if (!$(this).hasClass("disabled")) {
      var input = $(this).siblings("input");
      var min = input.attr("min");

      $(input)
        .val(function (index, value) {
          if (parseInt(value) > min) {
            return parseInt(value) - 1;
          } else if (parseInt(value) <= min) {
            return 1;
          }
        })
        .change();
    }
  });

  $(document).on("click", "button.add", function (event) {
    event.preventDefault();

    if (!$(this).hasClass("disabled")) {
      var input = $(this).siblings("input");
      var max = input.attr("max");

      $(this)
        .siblings("input")
        .val(function (index, value) {
          if (parseInt(value) < max) {
            return parseInt(value) + 1;
          } else if (parseInt(value) >= max) {
            return max;
          }
        })
        .change();
    }
  });

  $(document).on("change", ".product-counter input", function () {
    var value = parseInt($(this).val());
    var min = $(this).attr("min");
    min = min == 0 ? 1 : min;
    var max = $(this).attr("max");

    if (value <= min) {
      $(this).val(min);
      $(this).siblings("button.add.disabled").removeClass("disabled");
      $(this).siblings("button.sub:not(.disabled)").addClass("disabled");
    } else if (value > min && value < max) {
      $(this).siblings("button.disabled").removeClass("disabled");
    } else if (value >= max) {
      $(this).val(max);
      $(this).siblings("button.sub.disabled").removeClass("disabled");
      $(this).siblings("button.add:not(.disabled)").addClass("disabled");
    }
  });

  $(document).on(
    "click",
    ".add-to-cart-wrapper button.add-to-cart:not(.custom-box)",
    function (event) {
      $(event.target).addClass("loading");
      $(document.body).trigger("adding_to_cart", [$(this), $(this).data]);
      var qty = $(this).parent().find(".product-counter input").val();
      var product_id =
        $(".variations_form .variation_id").val() ?? $(this).data("id");

      var settings = {
        url: "/?add-to-cart=" + product_id + "&quantity=" + qty,
        method: "GET",
        timeout: 0,
      };

      $.ajax(settings).done(function (response) {
        $(event.target).removeClass("loading");
        $(document.body).trigger("added_to_cart", [
          response.fragments,
          response.cart_hash,
          $(this),
        ]);
        updateCartTotal(true, true);
      });
    }
  );
}

function updateCart() {
  var timeout;

  $(document).on("change", ".product-counter input.qty", function () {
    if (timeout !== undefined) {
      clearTimeout(timeout);
    }

    const name = $(this).attr("name");
    const quantity = $(this).val();

    if ($(this).parents(".sidecart").length) {
      timeout = setTimeout(function () {
        var settings = {
          url:
            "/wp-admin/admin-ajax.php?action=remove_cart_item&cart_item_key=" +
            name +
            "&quantity=" +
            quantity,
          method: "GET",
          timeout: 0,
        };

        $.ajax(settings).done(function () {
          updateSidecart();
        });
      }, 500);
    } else {
      $(".update-cart button[name='update_cart']").trigger("click");
    }
  });
}
