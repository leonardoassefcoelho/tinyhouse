$ = jQuery.noConflict();

jQuery.event.special.touchstart = {
  setup: function (_, ns, handle) {
    this.addEventListener("touchstart", handle, {
      passive: !ns.includes("noPreventDefault"),
    });
  },
};
jQuery.event.special.touchmove = {
  setup: function (_, ns, handle) {
    this.addEventListener("touchmove", handle, {
      passive: !ns.includes("noPreventDefault"),
    });
  },
};
jQuery.event.special.wheel = {
  setup: function (_, ns, handle) {
    this.addEventListener("wheel", handle, { passive: true });
  },
};
jQuery.event.special.mousewheel = {
  setup: function (_, ns, handle) {
    this.addEventListener("mousewheel", handle, { passive: true });
  },
};

$(document).ready(function () {
  $(window).on("scroll", function () {
    if ($(window).scrollTop() > 50) {
      $("header .header").addClass("is-scrolled");
    } else {
      $("header .header").removeClass("is-scrolled");
    }
  });

  $("header .menu-button").on("click", function () {
    $("header").toggleClass("is-active");
    changeHeaderBackground();
  });

  $("html").on("click", function (event) {
    const menu = $("header");

    if ($(window).width() >= 1366) {
      if (!menu.is(event.target) && menu.has(event.target).length === 0) {
        if ($(".menu-item.has-children").hasClass("is-active")) {
          $(".menu-item.has-children.is-active").removeClass("is-active");
          changeHeaderBackground();
        }
      }
    }
  });

  $("header button.search").on("click", function () {
    $("header").toggleClass("is-active");
    $(".has-children").removeClass("is-active");
    changeHeaderBackground();
  });

  $(".menu-wrapper .has-children p").on("click", function () {
    $(this)
      .parent()
      .toggleClass("is-active")
      .siblings()
      .removeClass("is-active");
  });

  $(".header .has-children p").on("click", function () {
    $(this)
      .parent()
      .toggleClass("is-active")
      .siblings()
      .removeClass("is-active");
    $("header").removeClass("is-active");
    changeHeaderBackground();
  });

  $("header ul.sub-menu").slick({
    slidesToShow: 2.3,
    slidesToScroll: 2,
    arrows: false,
    infinite: false,
    mobileFirst: true,
    touchThreshold: 100,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3.3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 1365,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        },
      },
    ],
  });

  slickHeaderCategories();

  $(window).resize(function () {
    slickHeaderCategories();
  });
});

function changeHeaderBackground() {
  if ($(".header").hasClass("is-scrolled") == false) {
    $(".header").toggleClass("is-menu-open");
  }
}

function slickHeaderCategories() {
  if ($(window).width() >= 1366) {
    $("header .categories-wrapper").slick({
      slidesToShow: 4,
      slidesToScroll: 4,
      arrows: false,
      infinite: false,
      touchThreshold: 100,
    });
  }
}
