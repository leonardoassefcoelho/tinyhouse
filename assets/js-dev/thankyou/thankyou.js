$(document).ready(function () {
  $(".print-link").on("click", function (e) {
    e.preventDefault();
    var url = window.location.href;
    var arr = url.split("?");

    if (url.length > 1 && arr[1] !== "") {
      url = url + "&print";
    } else {
      url = url + "?print";
    }

    var printWindow = window.open(
      url,
      "Tinyhouse Receipt",
      "left=200, top=200, width=950, height=500, toolbar=0, resizable=0"
    );

    printWindow.addEventListener(
      "load",
      function () {
        if (Boolean(printWindow.chrome)) {
          printWindow.print();
          setTimeout(function () {
            printWindow.close();
          }, 500);
        } else {
          printWindow.print();
          printWindow.close();
        }
      },
      true
    );
  });
});
