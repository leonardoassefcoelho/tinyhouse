$(document).ready(function () {
  $(".contact-form").submit(function (e) {
    e.preventDefault();

    var form = $(this);
    var url = form.attr("action");
    var submitButton = $(".contact-form button[type='submit']");

    submitButton.addClass("loading");
    submitButton.attr("disabled", "disabled");

    $.ajax({
      type: "POST",
      url: url,
      data: form.serialize(),
      success: function (data) {
        submitButton.removeClass("loading");
        submitButton.removeAttr("disabled");

        $(".contact-form .button[type='submit']").text(
          "Thank you for your message."
        );
        setTimeout(() => {
          submitButton.text("Submit your message.");
        }, 2000);
      },
      error: function (error) {
        submitButton.removeClass("loading");
        submitButton.removeAttr("disabled");

        submitButton.text("Error. Try again.");
        setTimeout(() => {
          submitButton.text("Submit your message.");
        }, 2000);
      },
    });
  });
});
