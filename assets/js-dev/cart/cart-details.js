$(document).ready(function () {
	open_discount_field();

	$(document.body).on("updated_cart_totals", function () {
		open_discount_field();
	});
});

function open_discount_field() {
	$("tr.cart-discount-th th").on("click", function () {
		$(this).closest("tr.cart-discount-th").toggleClass("expanded");
	});
}