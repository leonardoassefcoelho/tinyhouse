$(document).ready(function () {
	$(".product-suggestions .products-wrapper").slick({
		prevArrow:
			'<button class="slick-prev slick-arrow" aria-label="Previous" type="button"></button>',
		nextArrow:
			'<button class="slick-next slick-arrow" aria-label="Previous" type="button"></button>',
		dots: false,
		mobileFirst: true,
		infinite: false,
		touchThreshold: 100,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				},
			},
			{
				breakpoint: 1366,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
				},
			},
		],
	});
});
