$(document).ready(function () {
  $(".question").on("click", function () {
    var id = $(this).attr("aria-id");

    $(".answer:not(#" + id + "), .question:not(#" + id + ")").attr(
      "aria-expanded",
      false
    );
    $("#" + id + ".answer, .question[aria-id='" + id + "']").attr(
      "aria-expanded",
      true
    );
  });
});
