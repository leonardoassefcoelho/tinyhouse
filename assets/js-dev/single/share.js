$(document).ready(function () {
  var clipboard = new ClipboardJS(".share.copy");

  clipboard.on("success", function () {
    $(".share.copy").addClass("copied");
    setTimeout(() => {
      $(".share.copy").removeClass("copied");
    }, 1000);
  });
});
