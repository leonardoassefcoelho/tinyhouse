$(document).ready(function () {
  $(".more-posts .slider-wrapper").slick({
    prevArrow:
      '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"></button>',
    nextArrow:
      '<button class="slick-next slick-arrow" aria-label="Previous" type="button"></button>',
    dots: true,
    mobileFirst: true,
    infinite: false,
    touchThreshold: 100,
    responsive: [
      {
        breakpoint: 767,
        settings: "unslick",
      },
    ],
  });
});
