$(document).ready(function () {
	$(".product-gallery").slick({
		touchThreshold: 100,
		prevArrow:
			'<button class="slick-prev slick-arrow" aria-label="Previous" type="button"></button>',
		nextArrow:
			'<button class="slick-next slick-arrow" aria-label="Previous" type="button"></button>',
		dots: false,
		mobileFirst: true,
		infinite: false,
	});
});
