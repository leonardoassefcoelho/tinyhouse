$(document).ready(function () {
    $('.related-products-section .products-wrapper').slick({
        prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"></button>',
        nextArrow: '<button class="slick-next slick-arrow" aria-label="Previous" type="button"></button>',
        dots: true,
        mobileFirst: true,
        touchThreshold: 100,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 1365,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }
            }
        ]
    });
});