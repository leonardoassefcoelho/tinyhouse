$(document).ready(function () {
    const autoplay = $(".home-hero").hasClass("autoplay");
    const autoplayDelay = $(".home-hero").data("autoplay");

    $('.home-hero').slick({
        prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"></button>',
        nextArrow: '<button class="slick-next slick-arrow" aria-label="Previous" type="button"></button>',
        dots: true,
        touchThreshold: 100,
        autoplay: autoplay ? true : false,
        autoplaySpeed: autoplayDelay,
    });
});