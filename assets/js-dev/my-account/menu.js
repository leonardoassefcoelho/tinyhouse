$(document).ready(function () {
	$(".woocommerce-MyAccount-navigation ul").slick({
		arrows: false,
		mobileFirst: true,
		slidesToShow: 1.5,
		slidesToScroll: 1,
		infinite: false,
		focusOnSelect: true,
		accessibility: true,
		touchThreshold: 100,
		responsive: [
			{
				breakpoint: 399,
				settings: {
					slidesToShow: 2.5,
					slidesToScroll: 2,
				},
			},
			{
				breakpoint: 699,
				settings: {
					slidesToShow: 3.5,
					slidesToScroll: 3,
				},
			},
			{
				breakpoint: 1365,
				settings: {
					settings: "unslick",
				},
			}
		],
	});

	$(".shop-menu .menu-item").on("click", function (e) {
		$(".shop-menu .menu-item.active").removeClass("active");
		$(this).addClass("active");

		e.preventDefault();

		var section = $(this).children(".menu-item-url").attr("href");

		$("html,body").animate(
			{
				scrollTop: $(section).offset().top,
			},
			"slow"
		);
	});

	if(window.location.hash) {
		$(".shop-menu .menu-item.active").removeClass("active");

		$(".shop-menu .menu-item a[href='" + window.location.hash + "']").parent().addClass("active");
	}

	var stickyOffset = $(".shop-menu").offset().top;

	$(window).scroll(function () {
		var sticky = $(".shop-menu"),
			scroll = $(window).scrollTop();

		if (scroll >= stickyOffset) {
			sticky.addClass("fixed");
			$(".shop-grid").css("margin-top", sticky.height());
		} else {
			sticky.removeClass("fixed");
			$(".shop-grid").css("margin-top", 0);
		}
	});
});
