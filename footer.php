    <?php wp_footer(); ?>
    <footer class="padding-container">
        <div class="footer-start-wrapper">
            <?php
            $footer_logo = get_field('footer-logo', 'option');
            if ($footer_logo) :
                echo file_get_contents($footer_logo);
            endif;
            ?>
            <div class="social-wrapper">
                <div class="newsletter-wrapper">
                    <p class="title"><?= get_field('footer-newsletter-title', 'option') ?></p>
                    <p class="description"><?= get_field('footer-newsletter-description', 'option') ?></p>
                    <form class="newsletter-form" action="#">
                        <input type="email" placeholder="E-Mail">
                        <input class="button light default" type="submit" value="Subscribe">
                    </form>
                    <p class="final-text"><?= get_field('footer-newsletter-final-text', 'option') ?></p>
                </div>
                <div class="social-networks-wrapper">
                    <?php
                    $social_networks = get_option('wpseo_social');
                    ?>
                    <?php if (!empty($social_networks['instagram_url'])) : ?>
                        <a href="<?= $social_networks['instagram_url'] ?>" alt="Instagram" rel="nofollow">Instagram</a>
                    <?php endif; ?>
                    <?php if (!empty($social_networks['facebook_site'])) : ?>
                        <a href="<?= $social_networks['facebook_site'] ?>" alt="Facebook" rel="nofollow">Facebook</a>
                    <?php endif; ?>
                    <?php if (!empty($social_networks['twitter_site'])) : ?>
                        <a href="<?= $social_networks['twitter_site'] ?>" alt="Twitter" rel="nofollow">Twitter</a>
                    <?php endif; ?>
                </div>
            </div>
            <?php
            wp_nav_menu(array(
                'menu'           => 'Footer Menu',
                'theme_location' => 'footer-menu',
                'fallback_cb'    => false,
                'container_class'        => "footer-menu",
            ));
            ?>

        </div>
        <div class="footer-end-wrapper">
            <?php
            wp_nav_menu(array(
                'menu'           => 'Legal Menu',
                'theme_location' => 'legal-menu',
                'fallback_cb'    => false,
                'container_class'        => "legal-menu",
            ));
            ?>
            <div class="copyright">
                <p>® TINY HOUSE OF CHOCOLATE ARTISANS. <?= date('Y') ?>.</p>
            </div>
        </div>
    </footer>
    </body>

    </html>