<?php
get_header();
?>

<div class="author-content">
    <?php get_template_part('template-parts/author/hero');  ?>
    <?php get_template_part('template-parts/author/posts-wrapper');  ?>
</div>

<?php
get_footer();
