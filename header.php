<!DOCTYPE html>
<html <?= get_language_attributes() ?>>

<head>
    <?php
    $template_directory_uri = get_template_directory_uri();
    $gtm_id = get_field("gtm-id", "option");
    $cart_items_quantity = WC()->cart->get_cart_contents_count();
    $header_logo = get_field("header-logo", "option");

    if ($gtm_id) :
    ?>
        <!-- Google Tag Manager -->
        <script>
            (function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', '<?= $gtm_id ?>');
        </script>
        <!-- End Google Tag Manager -->
    <?php
    endif;
    ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/svg+xml" href="<?= $template_directory_uri ?>/assets/favicon/favicon.svg">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $template_directory_uri ?>/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $template_directory_uri ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $template_directory_uri ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= $template_directory_uri ?>/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?= $template_directory_uri ?>/assets/favicon/safari-pinned-tab.svg" color="#333333">
    <link rel="shortcut icon" href="<?= $template_directory_uri ?>/assets/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#d3b985">
    <meta name="msapplication-config" content="<?= $template_directory_uri ?>/assets/favicon/browserconfig.xml">
    <meta name="theme-color" content="#d3b985">
    <?php wp_head() ?>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Organization",
            "url": "<?= home_url() ?>",
            "logo": "<?= $header_logo['url'] ?>"
        }
    </script>
</head>

<body <?php body_class() ?>>
    <?php
    if ($gtm_id) :
    ?>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $gtm_id ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    <?php
    endif;
    ?>
    <header class="<?= get_field('header-style', 'option') ?>">
        <div class="padding-container header">
            <button class="menu-button mobile" aria-label="Menu">
                <span class="menu-part"></span>
                <span class="menu-part"></span>
                <span class="menu-part"></span>
            </button>
            <?php if (is_front_page()) : ?>
                <h1 class="logo">
                <?php else : ?>
                    <a class="logo" href="<?= home_url() ?>" aria-label="Home">
                    <?php endif; ?>
                    <?php
                    if ($header_logo) :
                        echo file_get_contents($header_logo['url']);
                    endif;
                    ?>
                    <?php if (is_front_page()) : ?>
                </h1>
            <?php else : ?>
                </a>
            <?php endif; ?>
            <nav class="header-menu desktop" aria-label="Main menu">
                <h2 class="hidden">
                    Main Menu
                </h2>
                <ul class="menu">
                    <?php
                    $header_nav = wp_get_menu_array('header-menu');

                    foreach ($header_nav as $menu_item) :
                        if (empty($menu_item['children'])) :
                    ?>
                            <li class="menu-item">
                                <a href="<?= $menu_item['url'] ?>" alt="<?= $menu_item['title'] ?>"><?= $menu_item['title'] ?></a>
                            </li>
                        <?php
                        else :
                        ?>
                            <li class="menu-item has-children">
                                <p><?= $menu_item['title'] ?></a>
                                <div class="sub-menu-wrapper">
                                    <div class="sub-menu-header">
                                        <h2 class="h2"><?= $menu_item['description'] ?></h2>
                                        <?php if (get_option('woocommerce_shop_page_id') == $menu_item['object_id']) : ?>
                                            <a href="<?= wc_get_page_permalink('shop') ?>" alt="Shop all">Shop all</a>
                                        <?php endif; ?>
                                    </div>
                                    <ul class="sub-menu">
                                        <?php foreach ($menu_item['children'] as $children) : ?>
                                            <li class="menu-item sub-menu-item">
                                                <?php
                                                if ($children['object'] == 'product_cat') :
                                                    $thumbnail_id = get_term_meta($children['object_id'], 'thumbnail_id', true);
                                                    $categoryImage = tinyhouse_image_sanitize($thumbnail_id, "medium");
                                                ?>
                                                    <div class="img-wrapper">
                                                        <img loading="lazy" src="<?= $categoryImage['src'] ?>" alt="<?= $categoryImage['alt'] ?>" title="<?= $categoryImage['title'] ?>" <?= ($categoryImage['srcset'] ? 'srcset="' . $categoryImage['srcset'] . '"' : '') ?>>
                                                    </div>
                                                <?php endif; ?>
                                                <a href="<?= $children['url'] ?>" alt="<?= $children['title'] ?>"><?= $children['title'] ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </li>
                    <?php
                        endif;
                    endforeach;
                    ?>
                </ul>
            </nav>
            <div class="right-icons-wrapper">
                <a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" title="<?= is_user_logged_in() ? 'My Account' : 'Login' ?>"><?= is_user_logged_in() ? 'My Account' : 'Login' ?></a>
                <button class="search" aria-label="Search">
                    <?= file_get_contents($template_directory_uri . "/assets/icons/search.svg") ?>
                </button>
                <button class="cart" aria-label="Cart">
                    <?= file_get_contents($template_directory_uri . "/assets/icons/cart.svg") ?>
                    <span class="cart-count"><?= $cart_items_quantity ?></span>
                </button>
            </div>
        </div>

        <div class="padding-container menu-wrapper">
            <div class="search-wrapper">
                <form id="searchform" method="get" action="<?= esc_url(home_url('/')); ?>">
                    <input type="text" class="search-field" name="s" placeholder="Search" value="<?php echo get_search_query(); ?>">
                    <button aria-label="Search"><?= file_get_contents($template_directory_uri . "/assets/icons/search.svg") ?></button>
                </form>
            </div>
            <nav class="header-menu mobile">
                <h2 class="hidden">
                    Main Menu
                </h2>
                <ul class="menu">
                    <?php
                    $header_nav = wp_get_menu_array('header-menu');

                    foreach ($header_nav as $menu_item) :
                        if (empty($menu_item['children'])) :
                    ?>
                            <li class="menu-item">
                                <a href="<?= $menu_item['url'] ?>" alt="<?= $menu_item['title'] ?>"><?= $menu_item['title'] ?></a>
                            </li>
                        <?php
                        else :
                        ?>
                            <li class="menu-item has-children">
                                <p><?= $menu_item['title'] ?></a>
                                <div class="sub-menu-wrapper">
                                    <div class="sub-menu-header">
                                        <h2 class="h2"><?= $menu_item['description'] ?></h2>
                                        <?php if (get_option('woocommerce_shop_page_id') == $menu_item['object_id']) : ?>
                                            <a href="<?= wc_get_page_permalink('shop') ?>" alt="Shop all">Shop all</a>
                                        <?php endif; ?>
                                    </div>
                                    <ul class="sub-menu">
                                        <?php foreach ($menu_item['children'] as $children) : ?>
                                            <li class="menu-item sub-menu-item">
                                                <?php
                                                if ($children['object'] == 'product_cat') :
                                                    $thumbnail_id = get_term_meta($children['object_id'], 'thumbnail_id', true);
                                                    $categoryImage = tinyhouse_image_sanitize($thumbnail_id, "medium");
                                                ?>
                                                    <div class="img-wrapper">
                                                        <img loading="lazy" src="<?= $categoryImage['src'] ?>" alt="<?= $categoryImage['alt'] ?>" title="<?= $categoryImage['title'] ?>" <?= ($categoryImage['srcset'] ? 'srcset="' . $categoryImage['srcset'] . '"' : '') ?>>
                                                    </div>
                                                <?php endif; ?>
                                                <a href="<?= $children['url'] ?>" alt="<?= $children['title'] ?>"><?= $children['title'] ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </li>
                    <?php
                        endif;
                    endforeach;
                    ?>
                    <li class="menu-item my-account">
                        <a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" title="<?= is_user_logged_in() ? 'My Account' : 'Login' ?>"><?= is_user_logged_in() ? 'My Account' : 'Login' ?></a>
                    </li>
                </ul>
            </nav>
            <?php if (!empty(get_field('highlighted-categories', 'option'))) : ?>
                <div class="categories-wrapper desktop">
                    <?php foreach (get_field('highlighted-categories', 'option') as $category) : ?>
                        <div class="category-item">
                            <?php
                            $thumbnail_id = get_term_meta($category->term_id, 'thumbnail_id', true);
                            $categoryImage = tinyhouse_image_sanitize($thumbnail_id, "medium");
                            ?>
                            <div class="img-wrapper">
                                <img loading="lazy" src="<?= $categoryImage['src'] ?>" alt="<?= $categoryImage['alt'] ?>" title="<?= $categoryImage['title'] ?>" <?= ($categoryImage['srcset'] ? 'srcset="' . $categoryImage['srcset'] . '"' : '') ?>>
                            </div>
                            <a href="<?= get_category_link($category->term_id) ?>" alt="<?= $category->name ?>"><?= $category->name ?></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </header>
    <?php if (!is_cart() && !is_checkout()) : ?>
        <div class="sidecart">
            <div class="wrapper">
                <div class="head">
                    <h2 class="title">Shopping Cart</h2>
                    <button class="close-btn" aria-label="Close sidecart"></button>
                </div>
                <?php
                if ($cart_items_quantity == 0) :
                ?>
                    <div class="empty-cart">
                        <p class="notice">Your cart is empty</p>
                        <p class="text">Return to home page or choose other products</p>
                    </div>
                <?php
                endif;
                ?>
                <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
                    <tbody>
                        <?php
                        $cart = "";

                        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {

                            $_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                            $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                            if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                                $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);

                                $cart .= '<tr class="woocommerce-cart-form__cart-item ' . esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)) . '">';
                                $cart .= '<td class="product-details">';

                                $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                                if (!$product_permalink) {
                                    $cart .= '<div class="product-thumbnail">' . $thumbnail . '</div>';
                                } else {
                                    $cart .= '<a class="product-thumbnail" href="' . esc_url($product_permalink) . '">' . $thumbnail . '</a>';
                                }

                                $cart .= '<div class="product-resume">';

                                if (!$product_permalink) {
                                    $cart .= wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<span class="product-name">%s</span>', $_product->get_name()), $cart_item, $cart_item_key));
                                } else {
                                    $cart .= wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a class="product-name" href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
                                }

                                $cart .= do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

                                $cart .= wc_get_formatted_cart_item_data($cart_item);

                                if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                    $cart .= wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
                                }

                                $cart .= apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);

                                if ($_product->is_sold_individually()) {
                                    $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                                } else {
                                    $product_quantity = woocommerce_quantity_input(
                                        array(
                                            'input_name'   => "cart[{$cart_item_key}][qty]",
                                            'input_value'  => $cart_item['quantity'],
                                            'max_value'    => $_product->get_max_purchase_quantity(),
                                            'min_value'    => '0',
                                            'product_name' => $_product->get_name(),
                                        ),
                                        $_product,
                                        false
                                    );
                                }

                                $cart .= apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item);

                                $cart .= apply_filters(
                                    'woocommerce_cart_item_remove_link',
                                    sprintf(
                                        '<button data-url="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">%s</button>',
                                        esc_url(wc_get_cart_remove_url($cart_item_key)),
                                        esc_html__('Remove item', 'woocommerce'),
                                        esc_attr($product_id),
                                        esc_attr($_product->get_sku()),
                                        esc_html__('Remove item', 'woocommerce')
                                    ),
                                    $cart_item_key
                                );

                                $cart .= '</div></td>';
                                $cart .= '<td class="product-subtotal" data-title="' . esc_attr('Subtotal', 'woocommerce') . '">';
                                $cart .= apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                                $cart .= '</td></tr>';
                            }
                        }

                        echo $cart;

                        ?>
                    </tbody>
                </table>
                <div class="infos">
                    <div class="subtotal">
                        <p><?php esc_html_e('Subtotal', 'woocommerce'); ?></p>
                        <p data-title="<?php esc_attr_e('Subtotal', 'woocommerce'); ?>"><?php wc_cart_totals_subtotal_html(); ?></p>
                    </div>
                    <p class="info">Proceed to checkout for shipping options</p>
                    <button class="button light default close-btn">Continue Shopping</button>
                    <a class="button dark default brown" href="<?= wc_get_cart_url() ?>">CART & SHIPPING</a>
                    <a class="button dark default" href="<?= wc_get_checkout_url() ?>">Checkout</a>
                </div>
            </div>
        </div>
    <?php endif; ?>