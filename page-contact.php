<?php
/* Template Name: Page Contact */
get_header();

?>

<div class="contact-content">
    <?php get_template_part('template-parts/contact/hero');  ?>
    <?php get_template_part('template-parts/contact/form');  ?>

</div>

<?php
get_footer();
