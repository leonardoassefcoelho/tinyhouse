<?php
/* Template Name: Page Stockists */
get_header();
?>

<div class="stockists-content">
    <?php get_template_part('template-parts/stockists/stores'); ?>
</div>

<?php
get_footer();
