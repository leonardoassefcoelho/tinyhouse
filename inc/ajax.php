<?php
function cart_total_items()
{
    global $woocommerce;
    echo $woocommerce->cart->cart_contents_count;
    wp_die();
}

add_action('wp_ajax_nopriv_cart_total_items', 'cart_total_items');
add_action('wp_ajax_cart_total_items', 'cart_total_items');

function remove_cart_item()
{
    preg_match("/cart\[(.*?)\]/", $_GET['cart_item_key'], $cart_item_key);
    $quantity = $_GET['quantity'];
    
    if($cart_item_key){
       WC()->cart->set_quantity($cart_item_key[1], $quantity);
       return true;
    } 
    return false;
}

add_action('wp_ajax_nopriv_remove_cart_item', 'remove_cart_item');
add_action('wp_ajax_remove_cart_item', 'remove_cart_item');

function tinyhouse_sendmail()
{
    if (
        !isset($_POST['contact-nonce'])
        || !wp_verify_nonce($_POST['contact-nonce'], 'contact_form')
    ) {
        wp_send_json_error();
    } else {
        $to = "leoassefbrotas@gmail.com";
        $email = $_POST['email'];
        $name = $_POST['name'];
        $subject = $_POST['subject'];
        $message = $_POST['message'];

        if ($email && $name && $subject && $message) {
            $headers = 'From: ' . $email . "\r\n" . 'Reply-To: ' . $email;

            $email_sent = wp_mail($to, $name . " - " . $subject, $message, $headers);

            if ($email_sent) {
                wp_send_json_success(null, 200);
            } else {
                wp_send_json_error(null, 400);
            }
        }
    }

    die();
}

add_action('wp_ajax_nopriv_tinyhouse_sendmail', 'tinyhouse_sendmail');
add_action('wp_ajax_tinyhouse_sendmail', 'tinyhouse_sendmail');

function tinyhouse_get_stockists()
{
    if (
        !isset($_GET['stockists-nonce'])
        || !isset($_GET['page-id'])
        || !wp_verify_nonce($_GET['stockists-nonce'], 'stockists_page')
    ) {
        wp_send_json_error(null, 400);
    } else {
        $markers = [];
        $page_id = $_GET['page-id'];

        if (have_rows('stockists-stores-states-repeater',  $page_id)) :
            while (have_rows('stockists-stores-states-repeater',  $page_id)) : the_row();
                if (have_rows('stockists-stores-citys-repeater',  $page_id)) :
                    while (have_rows('stockists-stores-citys-repeater',  $page_id)) : the_row();
                        if (have_rows('stockists-stores-repeater',  $page_id)) :
                            while (have_rows('stockists-stores-repeater',  $page_id)) : the_row();
                                $markers[] = array(
                                    "longitude" => get_sub_field('stockists-store-longitude', $page_id),
                                    "latitude" => get_sub_field('stockists-store-latitude', $page_id)
                                );
                            endwhile;
                        endif;
                    endwhile;
                endif;
            endwhile;
        endif;
    }

    wp_send_json_success($markers, 200);
    die();
}

add_action('wp_ajax_nopriv_tinyhouse_get_stockists', 'tinyhouse_get_stockists');
add_action('wp_ajax_tinyhouse_get_stockists', 'tinyhouse_get_stockists');

function update_sidecart()
{
    $cart = "";

    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {

        $_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
        $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

        if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
            $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);

            $cart .= '<tr class="woocommerce-cart-form__cart-item ' . esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)) . '">';
            $cart .= '<td class="product-details">';

            $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

            if (!$product_permalink) {
                $cart .= '<div class="product-thumbnail">' . $thumbnail . '</div>';
            } else {
                $cart .= '<a class="product-thumbnail" href="' . esc_url($product_permalink) . '">' . $thumbnail . '</a>';
            }

            $cart .= '<div class="product-resume">';

            if (!$product_permalink) {
                $cart .= wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<span class="product-name">%s</span>', $_product->get_name()), $cart_item, $cart_item_key));
            } else {
                $cart .= wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a class="product-name" href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
            }

            $cart .= do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);        

            $cart .= wc_get_formatted_cart_item_data($cart_item);

            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                $cart .= wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
            }

            $cart .= apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
            
            if ($_product->is_sold_individually()) {
                $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
            } else {
                $product_quantity = woocommerce_quantity_input(
                    array(
                        'input_name'   => "cart[{$cart_item_key}][qty]",
                        'input_value'  => $cart_item['quantity'],
                        'max_value'    => $_product->get_max_purchase_quantity(),
                        'min_value'    => '0',
                        'product_name' => $_product->get_name(),
                    ),
                    $_product,
                    false
                );
            }

            $cart .= apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item);

            $cart .= apply_filters(
                'woocommerce_cart_item_remove_link',
                sprintf(
                    '<button data-url="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">%s</button>',
                    esc_url(wc_get_cart_remove_url($cart_item_key)),
                    esc_html__('Remove item', 'woocommerce'),
                    esc_attr($product_id),
                    esc_attr($_product->get_sku()),
                    esc_html__('Remove item', 'woocommerce')
                ),
                $cart_item_key
            );

            $cart .= '</div></td>';
            $cart .= '<td class="product-subtotal" data-title="' . esc_attr('Subtotal', 'woocommerce') . '">';
            $cart .= apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
            $cart .= '</td></tr>';
        }
    }

    $data['cart'] = $cart;
    $data["subtotal"] = WC()->cart->get_cart_subtotal();

    wp_send_json_success( $data, 200);
    wp_die();
}

add_action('wp_ajax_nopriv_update_sidecart', 'update_sidecart');
add_action('wp_ajax_update_sidecart', 'update_sidecart');
