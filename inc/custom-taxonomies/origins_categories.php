<?php
function custom_taxonomy_origins_categories()
{
    $labels = array(
        'name'                       => 'Origins Categories',
        'singular_name'              => 'Origin Category',
        'menu_name'                  => 'Origins Categories',
        'all_items'                  => 'All Origins Categories',
        'parent_item'                => 'Parent Origin Category',
        'parent_item_colon'          => 'Parent Origin Category:',
        'new_item_name'              => 'New Origin Category',
        'add_new_item'               => 'Add New Origin Category',
        'edit_item'                  => 'Edit Origin Category',
        'update_item'                => 'Update Origin Category',
        'separate_items_with_commas' => 'Separate Origin Category with commas',
        'search_items'               => 'Search Origin Categories',
        'add_or_remove_items'        => 'Add or remove Origin Categories',
        'choose_from_most_used'      => 'Choose from the most used Origin Categories',
    );
    $args = array(
        'labels'                     => $labels,
        'public'                     => true,
        'publicly_queryable'         => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'rewrite'                    => false,
        'show_in_rest' => true
    );
    register_taxonomy('origins_categories', 'origins', $args);
    register_taxonomy_for_object_type('origins_categories', 'origins');
}

add_action('init', 'custom_taxonomy_origins_categories');
