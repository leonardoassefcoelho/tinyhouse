<?php
function custom_taxonomy_product_labels() {
    $labels = array(
        'name'                       => 'Product Labels',
        'singular_name'              => 'Product Label',
        'menu_name'                  => 'Product Labels',
        'all_items'                  => 'All Product Labels',
        'parent_item'                => 'Parent Product Label',
        'parent_item_colon'          => 'Parent Product Label:',
        'new_item_name'              => 'New Product Label',
        'add_new_item'               => 'Add New Product Label',
        'edit_item'                  => 'Edit Product Label',
        'update_item'                => 'Update Product Label',
        'separate_items_with_commas' => 'Separate Product Label with commas',
        'search_items'               => 'Search Product Labels',
        'add_or_remove_items'        => 'Add or remove Product Labels',
        'choose_from_most_used'      => 'Choose from the most used Product Labels',
    );
    $args = array(
        'labels'                     => $labels,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
    );
    register_taxonomy('product_labels', 'product', $args);
    register_taxonomy_for_object_type('product_labels', 'product');
}

add_action('init', 'custom_taxonomy_product_labels');