<?php

function tinyhouse_image_sanitize($imageId, $thumbnail_size = "thumbnail") {
    if ($imageId == '') {
        return null;
    } else {
        $img_default_src = wp_get_attachment_image_src($imageId, $thumbnail_size);

        $img_default_url = $img_default_src[0];
        $img_width = $img_default_src[1];
        $img_height = $img_default_src[2];

        $img_alt = get_post_meta($imageId, '_wp_attachment_image_alt', true);
        $img_title = get_the_title($imageId);

        $srcset_default = wp_get_attachment_image_srcset($imageId, $thumbnail_size);

        $image_values = array(
            'src' => $img_default_url,
            'width' => $img_width,
            'height' => $img_height,
            'alt' => $img_alt,
            'title' => $img_title,
            'srcset' => $srcset_default
        );

        return $image_values;
    }
}
