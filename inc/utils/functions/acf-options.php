<?php

function tinyhouse_register_options_page() {
	if (function_exists('acf_add_options_page')) {
		acf_add_options_page(array(
			'page_title'    => 'Site Settings',
			'menu_title'    => 'Site Settings',
			'menu_slug'     => 'site-settings',
			'icon_url'      => 'dashicons-admin-settings'
		));
		
		acf_add_options_page(array(
			'page_title' 	=> 'Header Settings',
			'menu_title'	=> 'Header',
			'menu_slug' 	=> 'header-settings',
			'icon_url'		=> 'dashicons-text-page',
			'parent_slug'   => 'site-settings',
		));

		acf_add_options_page(array(
			'page_title' 	=> 'Footer Settings',
			'menu_title'	=> 'Footer',
			'menu_slug' 	=> 'footer-settings',
			'icon_url'		=> 'dashicons-text-page',
			'parent_slug'   => 'site-settings'
		));

		acf_add_options_page(array(
			'page_title' 	=> 'Search Settings',
			'menu_title'	=> 'Search',
			'menu_slug' 	=> 'search-settings',
			'icon_url'		=> 'dashicons-text-page',
			'parent_slug'   => 'site-settings'
		));

		acf_add_options_page(array(
			'page_title'    => 'Google Tag Manager',
			'menu_title'    => 'GTM',
			'menu_slug'     => 'gtm-settings',
			'icon_url'      => 'dashicons-admin-settings',
			'parent_slug'   => 'site-settings',
		));

		acf_add_options_page(array(
			'page_title'    => '404',
			'menu_title'    => '404',
			'menu_slug'     => 'not-found-settings',
			'icon_url'      => 'dashicons-admin-settings',
			'parent_slug'   => 'site-settings',
		));

		acf_add_options_page(array(
			'page_title'    => 'Products',
			'menu_title'    => 'Products',
			'menu_slug'     => 'products-settings',
			'icon_url'      => 'dashicons-admin-settings',
			'parent_slug'   => 'site-settings',
		));
	}
}

add_action('after_setup_theme', 'tinyhouse_register_options_page');
