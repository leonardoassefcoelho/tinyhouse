<?php
//Creates a default excerpt
function tinyhouse_truncate_excerpt($text)
{
    if (strlen($text) > 119) :
        $text = mb_substr($text, 0, 100);
    endif;

    $text .= '...';

    return $text;
}

add_filter('get_the_excerpt', 'tinyhouse_truncate_excerpt');

//Adds support for safe SVG Files.
function tinyhouse_add_svg_support($mimes)
{
    $mimes['svg'] = 'image/svg+xml';

    return $mimes;
}

add_filter('upload_mimes', 'tinyhouse_add_svg_support');

//Adds WooCommerce Shop page option for ACF Fields
add_filter('acf/location/rule_values/page_type', function ($choices) {
    $choices['woo_shop_page'] = 'WooCommerce Shop Page';
    return $choices;
});

add_filter('acf/location/rule_match/page_type', function ($match, $rule, $options) {
    if ($rule['value'] == 'woo_shop_page' && isset($options['post_id'])) {
        if ($rule['operator'] == '==') {
            $match = ($options['post_id'] == wc_get_page_id('shop'));
        }
        if ($rule['operator'] == '!=') {
            $match = ($options['post_id'] != wc_get_page_id('shop'));
        }
    }
    return $match;
}, 10, 3);

// Disables WooCommerce Default Stylesheets
add_filter('woocommerce_enqueue_styles', '__return_empty_array');
function slug_disable_woocommerce_block_styles()
{
    wp_dequeue_style('wc-block-style');
}
add_action('wp_enqueue_scripts', 'slug_disable_woocommerce_block_styles');
add_action('wp_print_styles', function () {
    wp_style_add_data('woocommerce-inline', 'after', '');
});

//Removes CPB default styles and recreates some
remove_action('wp_head', 'CPB_Custom_Product_Boxes_Frontend::cpb_styles');
function cpb_remove_styles()
{
    wp_dequeue_style('cpb-box-product-style');
    wp_dequeue_style('slick');
    wp_dequeue_style('slick-theme');
    wp_dequeue_style('dashicons');
    wp_dequeue_style('dashicons');
    wp_deregister_script('cpb-box-product-script');
    wp_register_script('cpb-box-product-script', get_template_directory_uri() . '/assets/js/single-product/custom-box.js');

    $product_id = apply_filters('cpb_box_edit_subscription_product_id', get_the_id());
    $product = wc_get_product($product_id);
    if ('product' == get_post_type($product_id) && $product->is_type('cpb_custom_product_boxes')) :
        $min = get_post_meta($product_id, 'cpb_product_boxes_min_items', true);
        $min = !empty($min) ? absint($min) : 1;
        $max = get_post_meta($product_id, 'cpb_product_boxes_max_items', true);
        $max = !empty($max) ? absint($max) : 1;
        $added = get_option('cpb_product_boxes_item_added_text');
        $added = !empty($added) ? esc_html__($added, 'wc-cpb') : esc_html__('Product item added successfully...', 'wc-cpb');
        $max_error = get_option('cpb_product_boxes_max_error_text');
        $max_error = !empty($max_error) ? esc_html__($max_error, 'wc-cpb') : esc_html__('You can not add more products.', 'wc-cpb');
        $max_qty_error = get_option('cpb_product_boxes_qty_error');
        $max_qty_error = !empty($max_qty_error) ? esc_html__($max_qty_error, 'wc-cpb') : esc_html__('Maximum item quantity is reached.', 'wc-cpb');
        $currency_position = !empty(get_option('woocommerce_currency_pos')) ? get_option('woocommerce_currency_pos') : 'left';
        $thousand_sep = !empty(get_option('woocommerce_price_thousand_sep')) ? get_option('woocommerce_price_thousand_sep') : ',';
        $decimal_sep = !empty(get_option('woocommerce_price_decimal_sep')) ? get_option('woocommerce_price_decimal_sep') : '.';
        $no_of_decimal = get_option('woocommerce_price_num_decimals');
        $no_of_decimal = (!empty($no_of_decimal) || 0 == $no_of_decimal) ? $no_of_decimal : 2;
        $box_item_click = get_option('cpb_product_boxes_item_click');
        $box_item_click = !empty($box_item_click) ? $box_item_click : 'redirect';
        $price_type = get_post_meta($product_id, 'cpb_product_boxes_pricing_type', true);
        $boxes_type = get_post_meta($product_id, 'cpb_product_boxes_type', true);

        wp_localize_script(
            'cpb-box-product-script',
            'cpbBox',
            array(
                'ajaxurl'    =>  admin_url('admin-ajax.php'),
                'product_id' => $product_id,
                'type'       =>  $product->is_type('cpb_custom_product_boxes'),
                'ajax_nonce' => wp_create_nonce('cpb_product_boxes_nonce'),
                'price'      => $product->get_price(),
                'price_type' => $price_type,
                'min'        => $min,
                'max'        => $max,
                'added'       => $added,
                'max_error'  => $max_error,
                'max_qty_error' => $max_qty_error,
                'boxes_type' => $boxes_type,
                'currency_symbol' => get_woocommerce_currency_symbol(),
                'currency_pos'    => esc_attr($currency_position),
                'thousand_sep'    => esc_attr($thousand_sep),
                'decimal_sep'     => esc_attr($decimal_sep),
                'no_of_decimal'   => esc_attr($no_of_decimal),
                'box_item_click'  => $box_item_click,
                'disable_btn_popup' => get_option('cpb_product_boxes_disable_btn'),
                'counter' => get_option('cpb_product_boxes_counter'),
                'counter_text' => get_option('cpb_product_boxes_counter_text')
            )
        );
    endif;
}
add_action('wp_enqueue_scripts', 'cpb_remove_styles', 100);

//Removes WP Emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');

//Creates an output for coupon codes
function tinyhouse_cart_totals_coupon_html($coupon)
{
    if (is_string($coupon)) {
        $coupon = new WC_Coupon($coupon);
    }

    $discount_amount_html = '';

    $amount               = WC()->cart->get_coupon_discount_amount($coupon->get_code(), WC()->cart->display_cart_ex_tax);
    $discount_amount_html = '-' . wc_price($amount);

    if ($coupon->get_free_shipping() && empty($amount)) {
        $discount_amount_html = __('Free shipping coupon', 'woocommerce');
    }

    $discount_amount_html = apply_filters('woocommerce_coupon_discount_amount_html', $discount_amount_html, $coupon);
    $coupon_html          = $discount_amount_html . ' <a href="' . esc_url(add_query_arg('remove_coupon', rawurlencode($coupon->get_code()), is_cart() ? wc_get_cart_url() :  wc_get_checkout_url())) . '" class="th-woocommerce-remove-coupon" data-coupon="' . esc_attr($coupon->get_code()) . '">' . __('Remove', 'woocommerce') . '</a>';

    echo wp_kses(apply_filters('woocommerce_cart_totals_coupon_html', $coupon_html, $coupon, $discount_amount_html), array_replace_recursive(wp_kses_allowed_html('post'), array('a' => array('data-coupon' => true))));
}

require_once 'functions/image-sanitizer.php';
require_once 'functions/acf-options.php';


//REMOVER BARRA
// add_filter('show_admin_bar', '__return_false');

function cpb_custom_cart_item_quantity($quantity, $cart_item_key)
{
    if (isset(WC()->cart->cart_contents[$cart_item_key]['cpb_custom_parent_id'])) {
        return WC()->cart->cart_contents[$cart_item_key]['quantity'];
    }
    return '<span>' . $quantity . '</span>';
}

remove_filter('woocommerce_cart_item_quantity',  array('CPB_Custom_Product_Boxes_Cart', 'cpb_custom_cart_item_quantity'), 1, 2);

function tinyhouse_cpb_custom_cart_item_quantity($quantity, $cart_item_key)
{
    if (isset(WC()->cart->cart_contents[$cart_item_key]['cpb_custom_parent_id'])) {
        $cpb_quantity =  WC()->cart->cart_contents[$cart_item_key]['quantity'];

        $html_output = '<span class="cpb-item-qty">Quantity: ' . $cpb_quantity . '</span>';

        return $html_output;
    }

    return $quantity;
}
add_filter('woocommerce_cart_item_quantity', 'tinyhouse_cpb_custom_cart_item_quantity', 1, 2);

// remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 ); 

// remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );

function tinyhouse_change_paypal_text($title, $gateway_id)
{
    if ('paypal' == $gateway_id) {
        $title = '';
    }

    if ('stripe' == $gateway_id) {
        $title = str_replace("(Stripe)", "", $title);
    }
    return $title;
}

add_filter('woocommerce_gateway_title', 'tinyhouse_change_paypal_text', 10, 2);

function tinyhouse_change_paypal_icon($icon_html, $gateway_id)
{
    if ('paypal' == $gateway_id) {
        return file_get_contents(get_template_directory_uri() . '/assets/icons/paypal.svg');
    }
    return '';
}

add_filter('woocommerce_gateway_icon', 'tinyhouse_change_paypal_icon', 10, 2);

add_filter('wc_stripe_show_payment_request_on_checkout', '__return_true');

function tinyhouse_print_class($classes)
{
    if (is_checkout() && !empty(is_wc_endpoint_url('order-received')) && isset($_GET['print'])) {
        $classes[] = 'is-print';
    }

    return $classes;
}

add_filter('body_class', 'tinyhouse_print_class');

function tinyhouse_product_category_redirect()
{
    if (function_exists('is_product_category') && is_product_category()) {
        $shop_page_url = get_permalink(woocommerce_get_page_id('shop'));;
        $category = get_queried_object();
        $catinfo = get_category($category);

        wp_redirect($shop_page_url . "#" . $catinfo->slug);
        exit();
    }
}

add_action('template_redirect', 'tinyhouse_product_category_redirect');

function tinyhouse_replace_text_shipping_to_delivery($package_name, $i, $package)
{
    if (is_checkout()) {
        return;
    }
}

add_filter('woocommerce_shipping_package_name', 'tinyhouse_replace_text_shipping_to_delivery', 10, 3);


function custom_wc_checkout_fields_no_label($fields)
{
    $fields['order']['order_comments']['label'] = 'Is this a gift? Please, type here your gift note and always remember to sign your name.';
    foreach ($fields as $category => $value) {
        foreach ($fields[$category] as $field => $property) {
            $fields[$category][$field]['placeholder'] = $fields[$category][$field]['label'];
            unset($fields[$category][$field]['label']);
        }
    }
    unset($fields['billing']['billing_company']);
    unset($fields['shipping']['shipping_company']);
    return $fields;
}

add_filter('woocommerce_checkout_fields', 'custom_wc_checkout_fields_no_label');

function wpb_comment_reply_text($link)
{
    return preg_replace("/(?<=>)[^<]*(?=<\/a>)/", file_get_contents(get_template_directory_uri() . "/assets/icons/reply.svg") . 'Answer', $link);
}
add_filter('comment_reply_link', 'wpb_comment_reply_text');

add_filter('jetpack_sharing_counts', '__return_false', 99);
add_filter('jetpack_implode_frontend_css', '__return_false', 99);

function placeholder_author_email_url_form_fields($fields)
{
    $replace_author = __('Your Name', 'yourdomain');
    $replace_email = __('Your Email', 'yourdomain');
    $commenter = wp_get_current_commenter();
    $req = get_option('require_name_email');
    $aria_req = ($req ? " aria-required='true'" : '');

    $fields['author'] = '<p class="comment-form-author">' . '<label for="author">' . __('Name', 'yourdomain') . '</label> ' . ($req ? '<span class="required">*</span>' : '') .
        '<input id="author" name="author" type="text" placeholder="' . $replace_author . '" value="' . esc_attr($commenter['comment_author']) . '" size="20"' . $aria_req . ' /></p>';

    $fields['email'] = '<p class="comment-form-email"><label for="email">' . __('Email', 'yourdomain') . '</label> ' .
        ($req ? '<span class="required">*</span>' : '') .
        '<input id="email" name="email" type="text" placeholder="' . $replace_email . '" value="' . esc_attr($commenter['comment_author_email']) .
        '" size="30"' . $aria_req . ' /></p>';

    unset($fields['url']);

    return $fields;
}
add_filter('comment_form_default_fields', 'placeholder_author_email_url_form_fields');

function placeholder_comment_form_field($fields)
{
    $replace_comment = __('Your Comment', 'yourdomain');

    $fields['comment_field'] = '<p class="comment-form-comment"><label for="comment">' . _x('Comment', 'noun') .
        '</label><textarea id="comment" name="comment" cols="45" rows="8" placeholder="' . $replace_comment . '" aria-required="true"></textarea></p>';

    return $fields;
}
add_filter('comment_form_defaults', 'placeholder_comment_form_field');

add_filter('wc_add_to_cart_message_html', '__return_false');

function hide_shipping_when_free_is_available($rates)
{
    $free = array();
    foreach ($rates as $rate_id => $rate) {
        if ('free_shipping' === $rate->method_id) {
            $free[$rate_id] = $rate;
            break;
        }
    }
    return !empty($free) ? $free : $rates;
}

add_filter('woocommerce_package_rates', 'hide_shipping_when_free_is_available', 100);

function async_scripts($url)
{
    if (strpos($url, '#asyncload') === false && strpos($url, '#deferload') === false) :
        return $url;
    elseif (is_admin()) :
        $to_replace = ['#asyncload', '#deferload'];
        return str_replace($to_replace, '', $url);
    else :
        if (strpos($url, '#asyncload')) :
            return str_replace('#asyncload', '', $url) . "' async='async";
        else: 
            return str_replace('#deferload', '', $url) . "' async='defer";
        endif;
    endif;
}

add_filter('clean_url', 'async_scripts', 11, 1);

add_filter( 'get_site_icon_url', '__return_false' );
