<?php

function tinyhouse_radios_cpt() {

    $labels = array(
        'name'                     => 'Radios',
        'singular_name'         => 'Radio',
        'menu_name'             => 'Radios',
        'name_admin_bar'         => 'Radios',
        'all_items'             => 'Todos os radios',
        'add_new_item'             => 'Adicionar novo radio',
        'add_new'                 => 'Adicionar novo',
        'new_item'                 => 'Novo radio',
        'edit_item'             => 'Editar radio',
        'update_item'             => 'Atualizar radio',
        'view_item'             => 'Vizualizar radio',
        'view_items'             => 'Vizualizar radios',
        'search_items'             => 'Buscar por radios',
        'not_found'             => 'Radio não encontrado',
        'not_found_in_trash'     => 'Radio não encontrado no lixo',
        'featured_image'         => 'Imagem de destaque',
        'set_featured_image'     => 'Selecionar imagem de destaque',
        'remove_featured_image' => 'Remover imagem de destaque',
        'use_featured_image'     => 'Usar imagem de destaque',
        'insert_into_item'         => 'Inserir no radio',
        'uploaded_to_this_item' => 'Atualizado para o radio',
        'items_list'             => 'Lista de radios',
        'items_list_navigation' => 'Navegação por lista de radios',
        'filter_items_list'     => 'Filtrar lista de radios',
    );
    $args = array(
        'label'                 => 'Radios',
        'description'             => 'Register Radios',
        'labels'                 => $labels,
        'menu_icon'             => 'dashicons-format-audio',
        'supports'                 => array( 'title', 'editor', 'revisions', 'author', 'excerpt', 'thumbnail', 'custom-fields', 'post-formats'),
        'public'                 => true,
        'show_ui'                 => true,
        'show_in_menu'             => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'             => true,
        'has_archive'             => false,
        'hierarchical'             => false,
        'exclude_from_search'     => true,
        'show_in_rest'             => true,
        'publicly_queryable'     => true,
        'capability_type'         => 'page'
    );
    register_post_type('radios', $args);
}
add_action('init', 'tinyhouse_radios_cpt', 0);
