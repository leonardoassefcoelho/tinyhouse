<?php

function tinyhouse_origins_cpt()
{

    $labels = array(
        'name'                     => 'Origins',
        'singular_name'         => 'Origin',
        'menu_name'             => 'Origins',
        'name_admin_bar'         => 'Origins',
        'all_items'             => 'All origins',
        'add_new_item'             => 'Add new origin',
        'add_new'                 => 'Add new',
        'new_item'                 => 'New origin',
        'edit_item'             => 'Edit origin',
        'update_item'             => 'Update origin',
        'view_item'             => 'View origin',
        'view_items'             => 'View origins',
        'search_items'             => 'Search for origins',
        'not_found'             => 'Origin not found',
        'not_found_in_trash'     => 'Origin not found in trash',
        'featured_image'         => 'Featured image',
        'set_featured_image'     => 'Select featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'     => 'Use featured image',
        'insert_into_item'         => 'Insert into origin',
        'uploaded_to_this_item' => 'Uploader to this origin',
        'items_list'             => 'Origins list',
        'items_list_navigation' => 'Origins itens navigation',
        'filter_items_list'     => 'Filter origins list',
    );
    $args = array(
        'label'                 => 'Origins',
        'description'             => 'Register Origins',
        'labels'                 => $labels,
        'menu_icon'             => 'dashicons-admin-site',
        'supports'                 => array('title', 'editor', 'revisions', 'thumbnail', 'custom-fields', 'post-formats'),
        'taxonomies' => array('origins_categories'),
        'public'                 => false,
        'show_ui'                 => true,
        'show_in_menu'             => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => false,
        'can_export'             => true,
        'has_archive'             => false,
        'hierarchical'             => false,
        'exclude_from_search'     => true,
        'show_in_rest'             => false,
        'publicly_queryable'     => true,
        'capability_type'         => 'page',
        'rewrite' => false,
        'show_in_rest' => true
    );
    register_post_type('origins', $args);
}
add_action('init', 'tinyhouse_origins_cpt', 0);
