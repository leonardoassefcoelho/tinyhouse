<?php

function tinyhouse_setup()
{
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'tinyhouse_setup');

function tinyhouse_enqueue()
{
	$template_directory_uri = get_template_directory_uri();

	wp_enqueue_script('slick', $template_directory_uri . '/assets/js/common/slick.js');
	wp_enqueue_script('common-js', $template_directory_uri . '/assets/js/common/common.js');
	wp_enqueue_script('utilities-js', $template_directory_uri . '/assets/js/common/utilities.js#deferload');

	if (is_front_page()) {
		//CSS enqueues
		wp_enqueue_style('home-css', $template_directory_uri . '/assets/css/home.css');

		//JS enqueues
		wp_enqueue_script('hero-js', $template_directory_uri . '/assets/js/home/hero.js');
		wp_enqueue_script('featured-categories-js', $template_directory_uri . '/assets/js/home/featured-categories.js');
		wp_enqueue_script('products-js', $template_directory_uri . '/assets/js/home/products.js');
		wp_enqueue_script('badges-js', $template_directory_uri . '/assets/js/home/badges.js');
		wp_enqueue_script('journals-js', $template_directory_uri . '/assets/js/home/journals.js');
	} else if (is_shop() || is_product_tag()) {
		//CSS enqueues
		wp_enqueue_style('shop-css', $template_directory_uri . '/assets/css/shop.css');

		//JS enqueues
		wp_enqueue_script('shop-menu-js', $template_directory_uri . '/assets/js/shop/menu.js');
		wp_enqueue_script('shop-grid-js', $template_directory_uri . '/assets/js/shop/grid.js');
	} else if (is_product()) {
		$product_type = wc_get_product(get_the_ID())->get_type();

		//CSS enqueues
		if ($product_type == "variable") :
			wp_enqueue_script('selectWoo');
			wp_enqueue_style('select2');

			wp_enqueue_style('variable-product-css', $template_directory_uri . '/assets/css/variable-product.css');
			wp_enqueue_script('variable-selectwoo-js', $template_directory_uri . '/assets/js/single-product/variable.js');

		elseif ($product_type == "cpb_custom_product_boxes") :
			wp_enqueue_style('custom-box-css', $template_directory_uri . '/assets/css/custom-box.css');
		else :
			wp_enqueue_style('simple-product-css', $template_directory_uri . '/assets/css/simple-product.css');
		endif;

		//JS enqueues
		wp_enqueue_script('shop-menu-js', $template_directory_uri . '/assets/js/shop/menu.js');
		wp_enqueue_script('single-product-gallery-js', $template_directory_uri . '/assets/js/single-product/gallery.js');
		wp_enqueue_script('single-product-related-products-js', $template_directory_uri . '/assets/js/single-product/related-products.js');
	} elseif (is_cart()) {

		if (sizeof(WC()->cart->get_cart()) <= 0) {
			wp_enqueue_style('empty-cart-css', $template_directory_uri . '/assets/css/empty-cart.css');

			wp_enqueue_script('empty-cart-js', $template_directory_uri . '/assets/js/cart/empty-cart.js');
		} else {
			wp_enqueue_style('cart-css', $template_directory_uri . '/assets/css/cart.css');
		}

		wp_enqueue_script('cart-resume-js', $template_directory_uri . '/assets/js/cart/cart-details.js');
	} elseif (is_account_page()) {

		wp_enqueue_style('my-account-css', $template_directory_uri . '/assets/css/my-account.css');

	} elseif (is_checkout()) {
		wp_enqueue_style('checkout-css', $template_directory_uri . '/assets/css/checkout.css');

		wp_enqueue_script('checkout-js', $template_directory_uri . '/assets/js/checkout/checkout.js');
		if (!empty(is_wc_endpoint_url('order-received'))) {
			wp_enqueue_style('thankyou-css', $template_directory_uri . '/assets/css/thankyou.css');

			wp_enqueue_script('thankyou-js', $template_directory_uri . '/assets/js/thankyou/thankyou.js');
		}
	} elseif (is_page_template('page-origins.php')) {
		wp_enqueue_style('origins-css', $template_directory_uri . '/assets/css/origins.css');

		wp_enqueue_script('origins-js', $template_directory_uri . '/assets/js/origins/origins.js');
	} elseif (is_page_template('page-contact.php')) {
		wp_enqueue_style('contact-css', $template_directory_uri . '/assets/css/contact.css');

		wp_enqueue_script('contact-js', $template_directory_uri . '/assets/js/contact/contact.js');
	} elseif (is_page_template('page-stockists.php')) {
		wp_enqueue_style('stockists-css', $template_directory_uri . '/assets/css/stockists.css');

		wp_enqueue_script('stockists-js', $template_directory_uri . '/assets/js/stockists/stockists.js');
		wp_enqueue_script('openlayers-js', $template_directory_uri . '/assets/js/stockists/ol.js');
		wp_enqueue_script('openlayers-ms-js', $template_directory_uri . '/assets/js/stockists/olms.js');
		wp_enqueue_script('map-js', $template_directory_uri . '/assets/js/stockists/map.js');
	} elseif ((is_home() || is_archive()) && !is_author()) {
		wp_enqueue_style('journals-css', $template_directory_uri . '/assets/css/journals.css');

		wp_enqueue_script('journals-posts-js', $template_directory_uri . '/assets/js/journals/posts.js');
	} elseif (is_page_template('page-radios.php')) {
		wp_enqueue_style('radios-css', $template_directory_uri . '/assets/css/radios.css');

		wp_enqueue_script('radios-post-js', $template_directory_uri . '/assets/js/radio/posts.js');
	} elseif (is_single()) {
		wp_enqueue_style('single-css', $template_directory_uri . '/assets/css/single.css');

		wp_enqueue_script('single-clipboard-js', $template_directory_uri . '/assets/js/single/clipboard.js');
		wp_enqueue_script('single-share-js', $template_directory_uri . '/assets/js/single/share.js');
		wp_enqueue_script('single-related-js', $template_directory_uri . '/assets/js/single/related-posts.js');

		wp_enqueue_script('comment-reply');
	} elseif (is_page_template("page-faq.php")) {
		wp_enqueue_style('faq-css', $template_directory_uri . '/assets/css/faq.css');

		wp_enqueue_script('faq-js', $template_directory_uri . '/assets/js/faq/faq.js');
	} elseif (is_page_template("page-about-us.php")) {
		wp_enqueue_style('about-us-css', $template_directory_uri . '/assets/css/about-us.css');

		wp_enqueue_script('about-js', $template_directory_uri . '/assets/js/about/about.js');
	} elseif (is_page_template("page-privacy.php")) {
		wp_enqueue_style('privacy-css', $template_directory_uri . '/assets/css/privacy.css');
	} elseif (is_404()) {
		wp_enqueue_style('404-css', $template_directory_uri . '/assets/css/404.css');
	} elseif (is_search()) {
		wp_enqueue_style('search-css', $template_directory_uri . '/assets/css/search.css');

		wp_enqueue_script('shop-grid-js', $template_directory_uri . '/assets/js/shop/grid.js');
	} elseif (is_author()) {
		wp_enqueue_style('author-css', $template_directory_uri . '/assets/css/author.css');

		wp_enqueue_script('author-grid-js', $template_directory_uri . '/assets/js/author/posts.js');
	} else {
		wp_enqueue_style('commons-css', $template_directory_uri . '/assets/css/commons.css');
	}
	
	wp_dequeue_script( 'wc-cart-fragments' ); 
}

add_action('wp_enqueue_scripts', 'tinyhouse_enqueue');

function tinyhouse_register_menus()
{
	register_nav_menus(array(
		'header-menu' => 'Header Menu',
		'footer-menu' => 'Footer Menu',
		'legal-menu'  => 'Legal Menu'
	));
}

add_action('init', 'tinyhouse_register_menus');

function tinyhouse_set_post_views($post_id)
{
	$count_key = 'post_view_count';
	$count = get_post_meta($post_id, $count_key, true);

	if ($count == '') {
		add_post_meta($post_id, $count_key, '0');
	} else {
		$count = intval($count);
		$count++;
		update_post_meta($post_id, $count_key, $count);
	}
}

function tinyhouse_track_post_views()
{
	if (is_single()) {
		$post_id = get_the_ID();
		tinyhouse_set_post_views($post_id);
	}
}

add_action('wp_head', 'tinyhouse_track_post_views');

function wp_get_menu_array($current_menu)
{

	$array_menu = wp_get_nav_menu_items($current_menu);
	$menu = array();
	foreach ($array_menu as $m) {
		if (empty($m->menu_item_parent)) {
			$menu[$m->ID] = array();
			$menu[$m->ID]['ID'] = $m->ID;
			$menu[$m->ID]['title'] = $m->title;
			$menu[$m->ID]['object_id'] = $m->object_id;
			$menu[$m->ID]['object'] = $m->object;
			$menu[$m->ID]['url'] = $m->url;
			$menu[$m->ID]['description'] = $m->description;
			$menu[$m->ID]['children'] = array();
		}
	}
	$submenu = array();
	foreach ($array_menu as $m) {
		if ($m->menu_item_parent) {
			$submenu[$m->ID] = array();
			$submenu[$m->ID]['ID'] = $m->ID;
			$submenu[$m->ID]['title'] = $m->title;
			$submenu[$m->ID]['object_id'] = $m->object_id;
			$submenu[$m->ID]['object'] = $m->object;
			$submenu[$m->ID]['url'] = $m->url;
			$submenu[$m->ID]['description'] = $m->description;
			$menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
		}
	}
	return $menu;
}

function tinyhouse_yoast_to_bottom()
{
	return 'low';
}

add_filter('wpseo_metabox_prio', 'tinyhouse_yoast_to_bottom');

add_filter('woocommerce_get_breadcrumb', function ($crumbs, $Breadcrumb) {
	$shop_page_id = wc_get_page_id('shop');

	if ($shop_page_id > 0 && !is_shop()) {
		$new_breadcrumb = [
			get_the_title($shop_page_id),
			get_permalink($shop_page_id)
		];

		if (is_product()) :
			$crumbs[1] = $new_breadcrumb;
		elseif (is_cart() || is_checkout()) :
			array_splice($crumbs, 1, 0, [$new_breadcrumb]);
		endif;
	}
	return $crumbs;
}, 10, 2);
